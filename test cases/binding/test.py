#import inspect

from bind import *

class test(object):
	def __init__(self):
		self._speed = "speedoriginal"
		self.minSpeed = "minSpeedoriginal"
		self.maxSpeed = "maxSpeedoriginal"
		self.desiredSpeed = "desiredSpeedoriginal"
		self.acceleration = "accelerationoriginal"
		self.turnRate = "turnRateoriginal"
		self.currentDirection = "currentDirectionoriginal"
		self.desiredDirection = "desiredDirectionoriginal"
		self.velocity = "velocityoriginal"
		self.desiredVelocity = "desiredVelocityoriginal"
		self.testo = None
		
	@property
	def speed(self):
		return self._speed
		
	@speed.setter
	def speed(self, value):
		self._speed = value

class otest(object):
	def get_testo(self):
		return None

	def get_speed(self):
		return str(self._o.field_speed)
	def set_speed(self, value):
		self._o.field_speed = value
	
	def get_minSpeed(self):
		return str(self._o.field_minSpeed)
	def set_minSpeed(self, value):
		self._o.field_minSpeed = value
	
	def get_maxSpeed(self):
		return str(self._o.field_maxSpeed)
	def set_maxSpeed(self, value):
		self._o.field_maxSpeed = value
	
	def get_desiredSpeed(self):
		return str(self._o.field_desiredSpeed)
	def set_desiredSpeed(self, value):
		self._o.field_desiredSpeed = value
	
	def get_acceleration(self):
		return str(self._o.field_acceleration)
	def set_acceleration(self, value):
		self._o.field_acceleration = value
	
	def get_turnRate(self):
		return str(self._o.field_turnRate)
	def set_turnRate(self, value):
		self._o.field_turnRate = value
	
	def get_currentDirection(self):
		return str(self._o.field_currentDirection)
	def set_currentDirection(self, value):
		self._o.field_currentDirection = value
	
	def get_currentDirectionCopy(self):
		return str(self._o.field_currentDirectionCopy)
	def set_currentDirectionCopy(self, value):
		self._o.field_currentDirectionCopy = value
	
	def get_desiredDirection(self):
		return str(self._o.field_desiredDirection)
	def set_desiredDirection(self, value):
		self._o.field_desiredDirection = value
	
	def get_velocity(self):
		return str(self._o.field_velocity)
	def set_velocity(self, value):
		self._o.field_velocity = value
	
	def get_desiredVelocity(self):
		return str(self._o.field_desiredVelocity)
	def set_desiredVelocity(self, value):
		self._o.field_desiredVelocity = value
		
	def performTest(self, t, field):
		if (getattr(t, field) != field):
			self.fail = True
			print "    ", field, " == ", getattr(t, field)
			
	def performTest2(self, t, field):
		if (not hasattr(t, field)):
			return
			
		if (getattr(t, field) != field + "original"):
			self.fail = True
			print "    ", field + "original", " == ", getattr(t, field)

	def addField(self, t, field, getter, setter):
		if (self.fail):
			return
	
		self.fields.append(field)
		
		bindProperty(t, field, getter, setter)
		
		for field in self.fields:
			self.performTest(t, field)
			
	def removeField(self, t, field, getter, setter):
		if (self.fail):
			return
	
		self.fields.remove(field)
		self.fields2.append(field)
		
		unbindProperty(t, field, getter, setter)
		
		for field in self.fields:
			self.performTest(t, field)
			
		for field in self.fields2:
			self.performTest2(t, field)

	def __init__(self, t):
		bind(t, "_o", self)
		
		self.fields = []
		self.fields2 = []
		self.fail = False
		
		self.field_speed = "speed"
		self.field_minSpeed = "minSpeed"
		self.field_maxSpeed = "maxSpeed"
		self.field_desiredSpeed = "desiredSpeed"
		self.field_acceleration = "acceleration"
		self.field_turnRate = "turnRate"
		self.field_currentDirection = "currentDirection"
		self.field_currentDirectionCopy = "currentDirectionCopy"
		self.field_desiredDirection = "desiredDirection"
		self.field_velocity = "velocity"
		self.field_desiredVelocity = "desiredVelocity"
		
		self.addField(t, "speed", self.get_speed, self.set_speed)
		self.addField(t, "minSpeed", self.get_minSpeed, self.set_minSpeed)
		self.addField(t, "maxSpeed", self.get_maxSpeed, self.set_maxSpeed)
		self.addField(t, "desiredSpeed", self.get_desiredSpeed, self.set_desiredSpeed)
		self.addField(t, "acceleration", self.get_acceleration, self.set_acceleration)
		self.addField(t, "turnRate", self.get_turnRate, self.set_turnRate)
		self.addField(t, "currentDirection", self.get_currentDirection, self.set_currentDirection)
		self.addField(t, "currentDirectionCopy", self.get_currentDirectionCopy, None)
		self.addField(t, "desiredDirection", self.get_desiredDirection, self.set_desiredDirection)
		self.addField(t, "velocity", self.get_velocity, self.set_velocity)
		self.addField(t, "desiredVelocity", self.get_desiredVelocity, self.set_desiredVelocity)
		
		bindProperty(t, "testo", self.get_testo, None)
		self.fields.append("testo")
		
		if (t.testo != None):
			self.fail = True
			print "    ", "testo", " == ", getattr(t, "testo")
		
	def onDestroy(self, t):
		self.removeField(t, "speed", self.get_speed, self.set_speed)
		self.removeField(t, "minSpeed", self.get_minSpeed, self.set_minSpeed)
		self.removeField(t, "maxSpeed", self.get_maxSpeed, self.set_maxSpeed)
		self.removeField(t, "desiredSpeed", self.get_desiredSpeed, self.set_desiredSpeed)
		self.removeField(t, "acceleration", self.get_acceleration, self.set_acceleration)
		self.removeField(t, "turnRate", self.get_turnRate, self.set_turnRate)
		self.removeField(t, "currentDirection", self.get_currentDirection, self.set_currentDirection)
		self.removeField(t, "currentDirectionCopy", self.get_currentDirectionCopy, None)
		self.removeField(t, "desiredDirection", self.get_desiredDirection, self.set_desiredDirection)
		self.removeField(t, "velocity", self.get_velocity, self.set_velocity)
		self.removeField(t, "desiredVelocity", self.get_desiredVelocity, self.set_desiredVelocity)
		
		unbindProperty(t, "testo", self.get_testo, None)
		self.fields.remove("testo")
		self.fields2.append("testo")
		
		if (t.testo != None):
			self.fail = True
			print "    ", "testo", " == ", getattr(t, "testo")
		
		unbind(t, "_o", self)

class rawr(object):
	def __init__(self, value):
		self.value = value
		
class rawr2(rawr):
	def __init__(self, value):
		rawr.__init__(self, value)
		
class rawr3(rawr):
	def __init__(self, value):
		rawr.__init__(self, value)
		
def get_value(self):
	return self._value

def set_value(self, value):
	self._value = value
		
r = rawr2(3)
r._value = r.value
bindProperty(r, "value", get_value, set_value)

r2 = rawr2(7)

print r.value
print r2.value

r2._value = r2.value
bindProperty(r2, "value", get_value, set_value)

#print r.value
#print r2.value

r2.value = 9

unbindProperty(r2, "value", get_value, set_value)

#print r2.value

unbindProperty(r, "value", get_value, set_value)

print r.value
print r2.value


#print r2.testo

'''
t = test()
t2 = test()

o = otest(t)
o.onDestroy(t)
o = otest(t)
o.onDestroy(t)
'''
