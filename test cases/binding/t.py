def addProperty(classInstance, attrName, fget, fset, fdel=None, \
              fdoc="this is magic"):
    """adds a property to an existing class """
    prop = property(fget, fset, fdel, fdoc)
    setattr(classInstance, attrName, prop)

def addClassMethod(classInstance, func, methName=None):
    """adds function as a new class method to an existing class.
       function should have self as first argument
    """
    name = methName or func.__name__
    setattr(classInstance, name, classmethod(func))

def classFromName(className):
    "returns the class instance object"
    return getattr(sys.modules['__main__'], className)

#==========  example usage ====================================================

if __name__ == "__main__":
    pass 

    #-- testing addMethod ------------------

    def func1(self):
        "some test function"
        return "Name is %s" % self._name
    
    def func2(self, comment=''):
        "some test function"
        return "Age is %s %s" % (self._age, comment)
    
    class Example(object):
        "some example class"
    
        def __init__(self, name, age):
            self._name = name
            self._age = age
    
    sarah = Example('Sarah', 5)
    josh = Example('Josh', 2)
    
    lucia = Example('Lucia', 20)
    
    #-- testing properties ------------------

    print len(None)
    
    def getAltName(self):
        return "*" + self._name + "*"
    
    def setAltName(self, val):
        self._name = val[1:-1]
    
    addProperty(Example, 'altName', getAltName, setAltName)
    print sarah.altName
    sarah.altName="*NEW-SARAH*"
    print sarah.altName
    bud = Example('Bud', 42)
    print bud.altName
    bud.altName="*The king of beers*"
    print bud.altName
    print "\n-----------------------\n"
