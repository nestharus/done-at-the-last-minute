#
#	This contains functions for working with units vectors, quaternions, and euler angles
#
#		rad2deg(rad)
#		deg2rad(deg)
#
#		euler2deg(euler)
#		euler2rad(euler)
#
#		directionX(yaw, pitch)
#		directionY(pitch)
#		directionZ(yaw, pitch)
#
#		vectorYaw(direction)
#		vectorPitch(direction)
#
#		euler(pitch, yaw, roll)
#		unitvec(x, y, z)
#
#		yaw2quaternion(yaw)
#		pitch2quaternion(pitch)
#		roll2quaternion(roll)
#
#		quaternion2euler(direction)
#		quaternion2unit(direction)
#
#		euler2quaternion(euler)
#		euler2unit(eulerVector)
#
#		unit2euler(unit, roll = 0)
#		unit2quaternion(unit)
#
#		quaternionBetween(currentQuaternion, targetQuaternion)
#
#		Rotation Functions
#		---------------------------
#
#			Note that if turn rate is less than 0, the entire turn is performed
#
#			eulerRotation(currentAngle, targetAngle, turnRate = -1)
#				-	single angle
#
#
#			-	the below return resulting orientation
#			quaternionRotation(currentQuaternion, targetQuaternion, turnRate = -1)
#
#			quaternionRotationPitch(currentQuaternion, targetPitch, turnRate = -1)
#			quaternionRotationYaw(currentQuaternion, targetYaw, turnRate = -1)
#			quaternionRotationRoll(currentQuaternion, targetRoll, turnRate = -1)
#

from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import Quaternion
from ogre.renderer.OGRE import Matrix3
from ogre.renderer.OGRE import Radian

import math

_mat = Matrix3()
_x = Radian(0)
_y = Radian(0)
_z = Radian(0)

def euler(pitch, yaw, roll):
	return Vector3(pitch, yaw, roll)
	
def unitvec(x, y, z):
	vec = Vector3(x, y, z)
	return vec/vec.length()

def rad2deg(rad):
	return rad*57.2957795131
	
def deg2rad(deg):
	return deg/57.2957795131

def directionX(yaw, pitch):
	return math.cos(pitch)*math.cos(yaw)
	
def directionY(pitch):
	return math.sin(pitch)
	
def directionZ(yaw, pitch):
	return math.cos(pitch)*math.sin(yaw)
	
def vectorYaw(direction):
	return math.atan2(direction.z, direction.x)
	
def vectorPitch(direction):
	return math.atan2(direction.y, math.sqrt(direction.x*direction.x + direction.z*direction.z))
	
def pitch2quaternion(pitch):
	return Quaternion(pitch, Vector3(0, 0, 1))
	
def yaw2quaternion(yaw):
	return Quaternion(yaw, Vector3(0, 1, 0))
	
def roll2quaternion(roll):
	return Quaternion(roll, Vector3(1, 0, 0))
	
def quaternion2euler(direction):
	direction.ToRotationMatrix(_mat)
	
	_mat.ToEulerAnglesXYZ(_x, _y, _z)
	
	return Vector3(_x.valueRadians(), _y.valueRadians(), _z.valueRadians())
	
def euler2unit(eulerVector):
	return Vector3(directionX(eulerVector.y, eulerVector.x), directionY(eulerVector.x), directionZ(eulerVector.y, eulerVector.x))

def euler2quaternion(euler):
	_mat.FromEulerAnglesXYZ(euler.x, euler.y, euler.z)
	quat = Quaternion(0, 0, 0, 0)
	quat.FromRotationMatrix(_mat)
	return quat
	
def unit2euler(unit, roll = 0):
	return Vector3(vectorPitch(unit), vectorYaw(unit), roll)
	
def euler2deg(euler):
	euler.x = rad2deg(euler.x)
	euler.y = rad2deg(euler.y)
	euler.z = rad2deg(euler.z)
	
	return euler
	
def euler2rad(euler):
	euler.x = deg2rad(euler.x)
	euler.y = deg2rad(euler.y)
	euler.z = deg2rad(euler.z)
	
	return euler
	
def quaternion2unit(direction):
	return euler2unit(quaternion2euler(direction))
	
def unit2quaternion(unit):
	return euler2quaternion(unit2euler(unit))
	
def eulerRotation(currentAngle, targetAngle, turnRate = -1):
	if (turnRate == 0):
		return currentAngle

	if (turnRate < 0):
		return targetAngle - currentAngle

	if (math.cos(currentAngle - targetAngle) < math.cos(turnRate)):
		if (0 < math.sin(currentAngle - targetAngle)):
			return -turnRate
		
		return turnRate
	
	return targetAngle - currentAngle
	
def quaternionBetween(currentQuaternion, targetQuaternion):
	return targetQuaternion*currentQuaternion.Inverse()

def quaternionRotation(currentQuaternion, targetQuaternion, turnRate = -1):
	if (currentQuaternion.equals(targetQuaternion, .01) or turnRate < 0):
		return quaternionBetween(currentQuaternion, targetQuaternion)
		
	if (turnRate == 0):
		return quaternionBetween(currentQuaternion, currentQuaternion)

	difference = 2*math.acos(currentQuaternion.Dot(targetQuaternion))
	
	if (turnRate > difference):
		turnRate = difference
		
	rotation = turnRate/difference
	
	#	this is the final orientation
	return Quaternion.Slerp(rotation, currentQuaternion, targetQuaternion, True)
	
def quaternionRotationPitch(currentQuaternion, targetPitch, turnRate = -1):
	return quaternionRotation(currentQuaternion, euler2quaternion(Vector3(targetPitch, 0, 0)), turnRate)
	
def quaternionRotationYaw(currentQuaternion, targetYaw, turnRate = -1):
	return quaternionRotation(currentQuaternion, euler2quaternion(Vector3(0, targetYaw, 0)), turnRate)
	
def quaternionRotationRoll(currentQuaternion, targetRoll, turnRate = -1):
	return quaternionRotation(currentQuaternion, euler2quaternion(Vector3(0, 0, targetRoll)), turnRate)
