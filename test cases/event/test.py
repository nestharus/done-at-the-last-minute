from event import Event

onInit = Event()
onTest = Event()
onClean = Event()

class O1(object):
	def onTest(self, tval, tval2):
		print "O1.test", tval, ", ", tval2
	
	def onInit(self):
		onInit.unregister(self, "onInit")
		print "O1.init test"
		
	def onClean(self):
		print "O1.clean test"
		
class O2(object):
	def onTest(self, tval, tval2):
		print "O2.test", tval, ", ", tval2
	
	def onInit(self):
		onInit.unregister(self, "onInit")
		print "O2.init test"
		
	def onClean(self):
		print "O2.clean test"
		
class O3(object):
	value = "meh"

	@staticmethod
	def onTest(tval, tval2):
		print "O3.test", tval, ", ", tval2, ", ", O3.value
	
	@staticmethod
	def onInit():
		onInit.unregister(O3, "onInit")
		print "O3.init test"
	
	@staticmethod
	def onClean():
		print "O3.clean test"
		
def F1(tval, tval2):
	print "F1 test ", tval, ", ", tval2
	
#def register(self, callback, callbackname = None):

o1 = O1()
o2 = O2()

onInit.register(o1, "onInit")
onInit.register(o2, "onInit")
onInit.register(O3, "onInit")

onTest.register(o1, "onTest")
onTest.register(o2, "onTest")
onTest.register(O3, "onTest")
onTest.register(F1)

onClean.register(o1, "onClean")
onClean.register(o2, "onClean")
onClean.register(O3, "onClean")

onInit.fire()
onInit.fire()
onTest.fire("t1", "t2")
onClean.fire()