import inspect

class Event(object):
	def __init__(self):
		self.callbacks = []
		self.remove = []
		
	def fire(self, *arguments):
		for callback in self.remove:
			self.callbacks.remove(callback)
			
		self.remove = []
			
		for callback in self.callbacks:
				callback(*arguments)
			
	def _getCallback(self, callback, callbackname = None):
		if (callbackname != None):
			if (hasattr(callback, callbackname)):
				callback = getattr(callback, callbackname)
				
				if (inspect.ismethod(callback) or inspect.isfunction(callback)):
					return callback
				else:
					return None
		else:
			return callback
			
	def register(self, callback, callbackname = None):
		caller = callback
		callback = self._getCallback(callback, callbackname)
		
		if (caller == callback):
			caller = None
		
		if (callback != None):
			self.callbacks.append(callback)
		
	def unregister(self, callback, callbackname = None):
		caller = callback
		callback = self._getCallback(callback, callbackname)
		
		if (caller == callback):
			caller = None
		
		if (not (callback in self.remove) and (callback in self.callbacks)):
			self.remove.append(callback)
			
	def hasCallbacks(self):
		return len(self.callbacks) > 0