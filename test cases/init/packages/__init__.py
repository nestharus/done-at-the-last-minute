import os

packages = []

for module in os.listdir(os.path.dirname(__file__)):
	delimiter = "/"
	
	if (os.name == "nt"):
		delimiter = "\\"
		
	if (module != '__init__.py' and module[-3:] == '.py'):
		__import__(module[:-3], locals(), globals())
	elif (os.path.isdir(os.path.dirname(__file__) + delimiter + module)):
		packages.append(module)
		
for package in packages:
	__import__(package, locals(), globals())
	
del module
