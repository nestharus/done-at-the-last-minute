import ogre.renderer.OGRE as ogre
import ogre.io.OIS as OIS

from scripts.engine import Engine
from scripts.managers.input import InputManager

class _Exit(object):
	def __init__(self):
		InputManager.register(self)
	
	def onKeyPressed(self, frameEvent):
		if (frameEvent.key == OIS.KC_ESCAPE):
			Engine.stop()

Exit = _Exit()