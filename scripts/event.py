#	
#	Event
#
#		Fields
#		------------------------------
#
#			post
#
#				if True, fire events in reverse order
#
#				should use on destruction events
#
#		Methods
#		------------------------------
#
#			fire(self, *arguments)
#				-	fire the event, passing arguments to all listeners
#
#				Example:		myEvent.fire("arg1", "arg2", arg3)
#
#			register(self, object, callbackname = None)
#				-	object refers to the thing to run
#					this can be a class, a method, or a function
#
#				-	callbackname refers to the name of the function
#					this is only set for classes
#					it is used to *optionally* register the class to the event
#					it will only register the clas if the method exists (static or non-static)
#
#			unregister(self, object, callbackname = None)
#				-	this unregisters from the event
#				-	it operates similarly to register
#

import inspect

class Event(object):
	#	initialization
	def __init__(self):
		self.callbacks = []
		self.remove = []
		self.post = False
	
	#	methods
	def fire(self, *arguments):
		for callback in self.remove:
			self.callbacks.remove(callback)
			
		self.remove = []

		iterator = self.callbacks
		if (self.post):
			iterator = reversed(iterator)
		
		for callback in iterator:
			callback(*arguments)
			
	def _getCallback(self, callback, callbackname = None):
		if (callbackname != None):
			if (hasattr(callback, callbackname)):
				callback = getattr(callback, callbackname)
				
				if (inspect.ismethod(callback) or inspect.isfunction(callback)):
					return callback
				else:
					return None
		else:
			return callback
			
	def register(self, callback, callbackname = None):
		caller = callback
		callback = self._getCallback(callback, callbackname)
		
		if (caller == callback):
			caller = None
		
		if (callback != None):
			self.callbacks.append(callback)
		
	def unregister(self, callback, callbackname = None):
		caller = callback
		callback = self._getCallback(callback, callbackname)
		
		if (caller == callback):
			caller = None
		
		if (not (callback in self.remove) and (callback in self.callbacks)):
			self.remove.append(callback)
