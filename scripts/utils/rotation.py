#
#	This contains functions for working with units vectors, quaternions, and euler angles
#
#		angles
#
#			rad2deg(radian)									-> radian			Done
#			deg2rad(degree)									-> degree			Done
#
#		euler vectors
#
#			Vector3(pitch, yaw, roll) 						-> eulerVector		Done
#			eulerVector(unitVector, roll) 					-> eulerVector		Done
#			unitVector2eulerVector(unitVector, roll = 0)	-> eulerVector		Done
#
#			quaternion2eulerVector(quaternion)				-> eulerVector
#			-	this is only partially implemented
#				it will result in gimbal lock
#			-	first and third angles form lock
#
#			eulerVectorYaw(unitVector)						-> yaw				Done
#			eulerVectorPitch(unitVector)					-> pitch			Done
#
#			euler2deg(eulerVector)							-> eulerVector		Done
#			euler2rad(eulerVector)							-> eulerVector		Done
#				-	these convert between degrees
#					and radians
#
#		unit vectors
#
#			Vector3(x, y, z)								-> unitVector		Done
#			vector.normalisedCopy()							-> unitVector		Done
#			eulerVector2unitVector(eulerVector)				-> unitVector		Done
#
#			quaternion2unitVector(quaternion)				-> unitVector		Partial
#			-	this is only partially implemented
#				it will result in gimbal lock
#			-	first and third angles form lock
#
#			unitVectorX(yaw, pitch)							-> x				Done
#			unitVectorY(pitch)								-> y				Done
#			unitVectorZ(yaw, pitch)							-> z				Done
#
#		quaternions
#
#			eulerVector2quaternion(eulerVector)				-> quaternion		Partial
#			-	this is only partially implemented
#				it will result in gimbal lock
#			-	first and third angles form lock
#
#			unitVector2quaternion(unitVector)				-> quaternion		Partial
#			-	this is only partially implemented
#				it will result in gimbal lock
#
#			yaw2quaternion(yaw)								-> quaternion		Done
#			pitch2quaternion(pitch)							-> quaternion		Done
#			roll2quaternion(roll)							-> quaternion		Done
#
#			quaternionBetween(quaternion1, quaternion2)		-> quaternion		Done
#				-	this will return the quaternion between
#					two quaternions, similar to the angle
#					between two angles
#			-	first and third angles form lock
#
#
#		Rotation Functions
#		---------------------------
#
#			Note that if turn rate is less than 0, the entire turn is performed
#
#				angleRotation(currentAngle, targetAngle, turnRateAngle = -1)											-> angleRotation			Done
#				eulerVectorRotation(currentEulerVector, targetEulerVector, eulerVectorTurnRate = Vector3(-1, -1, -1))	-> eulerVectorRotation		Done
#
#			Note that the following use slerp, so they will not work correctly
#
#				quaternionRotation(currentQuaternion, targetQuaternion, turnRate = -1)									-> quaternionRotation		Broken
#
#				quaternionRotationPitch(currentQuaternion, targetPitch, turnRate = -1)									-> quaternionRotation		Broken
#				quaternionRotationYaw(currentQuaternion, targetYaw, turnRate = -1)										-> quaternionRotation		Broken
#				quaternionRotationRoll(currentQuaternion, targetRoll, turnRate = -1)									-> quaternionRotation		Broken
#

from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import Quaternion
from ogre.renderer.OGRE import Matrix3
from ogre.renderer.OGRE import Radian

import math

_mat = Matrix3()
_x = Radian(0)
_y = Radian(0)
_z = Radian(0)

_RAD_DEG = 57.2957795131

#	degrees/radians
def rad2deg(radian):
	return radian*_RAD_DEG
	
def deg2rad(degree):
	return degree/_RAD_DEG

#	unit vector
def unitVectorX(yaw, pitch):
	return math.cos(pitch)*math.cos(yaw)
	
def unitVectorY(pitch):
	return math.sin(pitch)
	
def unitVectorZ(yaw, pitch):
	return math.cos(pitch)*math.sin(yaw)

#	euler vector
def eulerVectorYaw(unitVector):
	return math.atan2(unitVector.z, unitVector.x)
	
def eulerVectorPitch(unitVector):
	return math.atan2(unitVector.y, math.sqrt(unitVector.x*unitVector.x + unitVector.z*unitVector.z))
	
def eulerVector(unitVector, roll):
	return Vector3(eulerVectorPitch(unitVector), eulerVectorYaw(unitVector), roll)
	
#	unit vector to euler vector
def unitVector2eulerVector(unitVector, roll = 0):
	return Vector3(eulerVectorPitch(unitVector), eulerVectorYaw(unitVector), roll)

#	euler vector to quaternion
def pitch2quaternion(pitch):
	return Quaternion(pitch, Vector3(0, 0, 1))
	
def yaw2quaternion(yaw):
	return Quaternion(yaw, Vector3(0, 1, 0))
	
def roll2quaternion(roll):
	return Quaternion(roll, Vector3(1, 0, 0))
	
def eulerVector2quaternion(eulerVector):
	_mat.FromEulerAnglesYXZ(eulerVector.y, eulerVector.x, eulerVector.z)
	quat = Quaternion(0, 0, 0, 0)
	quat.FromRotationMatrix(_mat)
	return quat

#	euler vector to unit vector
def eulerVector2unitVector(eulerVector):
	return Vector3(unitVectorX(eulerVector.y, eulerVector.x), unitVectorY(eulerVector.x), unitVectorZ(eulerVector.y, eulerVector.x))
	
#	quaternion to euler vector
def quaternion2eulerVector(eulerVector):
	direction.ToRotationMatrix(_mat)
	
	_mat.ToEulerAnglesYXZ(_y, _y, _z)
	
	return Vector3(_x.valueRadians(), _y.valueRadians(), _z.valueRadians())

def eulerVector2deg(eulerVector):
	eulerVector = Vector3(eulerVector.x, eulerVector.y, eulerVector.z)

	eulerVector.x = rad2deg(eulerVector.x)
	eulerVector.y = rad2deg(eulerVector.y)
	eulerVector.z = rad2deg(eulerVector.z)
	
	return eulerVector
	
def eulerVector2rad(eulerVector):
	eulerVector.x = deg2rad(eulerVector.x)
	eulerVector.y = deg2rad(eulerVector.y)
	eulerVector.z = deg2rad(eulerVector.z)
	
	return eulerVector

def quaternion2unitVector(eulerVector):
	return eulerVector2unitVector(quaternion2eulerVector(eulerVector))
	
def unitVector2quaternion(unitVector):
	return eulerVector2quaternion(unitVector2eulerVector(unitVector))

def angleRotation(currentAngle, targetAngle, turnRateAngle = -1):
	if (turnRateAngle == 0):
		return 0

	if (turnRateAngle < 0):
		return targetAngle - currentAngle

	if (math.cos(currentAngle - targetAngle) < math.cos(turnRateAngle)):
		if (0 < math.sin(currentAngle - targetAngle)):
			return -turnRateAngle
		
		return turnRateAngle
	
	return targetAngle - currentAngle
	
def eulerVectorRotation(currentEulerVector, targetEulerVector, eulerVectorTurnRate = Vector3(-1, -1, -1)):
	x = angleRotation(currentEulerVector.x, targetEulerVector.x, eulerVectorTurnRate.x)
	y = angleRotation(currentEulerVector.y, targetEulerVector.y, eulerVectorTurnRate.y)
	z = angleRotation(currentEulerVector.z, targetEulerVector.z, eulerVectorTurnRate.z)
	
	return Vector3(x, y, z)

def quaternionBetween(currentQuaternion, targetQuaternion):
	currentQuaternion = Quaternion(currentQuaternion.w, currentQuaternion.x, currentQuaternion.y, currentQuaternion.z)
	return targetQuaternion*currentQuaternion.Inverse()

def quaternionRotation(currentQuaternion, targetQuaternion, turnRate = -1):
	if (currentQuaternion.equals(targetQuaternion, .01) or turnRate < 0):
		return quaternionBetween(currentQuaternion, targetQuaternion)
		
	if (turnRate == 0):
		return quaternionBetween(currentQuaternion, currentQuaternion)

	difference = 2*math.acos(currentQuaternion.Dot(targetQuaternion))
	
	if (difference < turnRate):
		turnRate = difference
		
	rotation = turnRate/difference
	
	if (rotation > .5):
		rotation = 1 - rotation
		
	return quaternionBetween(currentQuaternion, Quaternion.Slerp(rotation, currentQuaternion, targetQuaternion, True))
	
def quaternionRotationPitch(currentQuaternion, targetPitch, turnRate = -1):
	return quaternionRotation(currentQuaternion, pitch2quaternion(targetPitch), turnRate)
	
def quaternionRotationYaw(currentQuaternion, targetYaw, turnRate = -1):
	return quaternionRotation(currentQuaternion, yaw2quaternion(targetYaw), turnRate)
	
def quaternionRotationRoll(currentQuaternion, targetRoll, turnRate = -1):
	return quaternionRotation(currentQuaternion, roll2quaternion(targetRoll), turnRate)
