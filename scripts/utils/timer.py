#
#	Timer
#
#		self.callback
#		self.timeout
#
#		Timer(self, timeout, callback, *args)
#			-	callback is just a function
#
#		pause(self)
#		unpause(self)
#		restart(self)
#
#		setArgs(self, *args)
#		getArgs(self)

from scripts.engine import Engine

class Timer:
	def __init__(self, timeout, callback, *args):
		self.callback = callback
		self.timeout = timeout
		self._timeout = self.timeout
		self.args = args
		self._remainingTime = self.timeout

		Engine.register(self)

	def pause(self):
		Engine.unregister(self)

	def unpause(self):
		Engine.register(self)

	def restart(self):
		self._remainingTime = self.timeout

	def setArgs(self, *args):
		self.args = args

	def getArgs(self):
		return self.args

	def onTick(self, dtime):
		if (self._timeout != self.timeout):
			self._remainingTime = self._remainingTime + self.timeout - self._timeout
			self.timeout = self._timeout
			
		self._remainingTime -= dtime

		if (self.remainingTime <= self.timeout):
			self.callback(*args)
			self._remainingTime = self.timeout
			self._timeout = self.timeout
