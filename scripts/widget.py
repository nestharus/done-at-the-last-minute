#---------------------------------------------------------------------------
# Copyright 2010, 2011 Sushil J. Louis and Christopher E. Miles, 
# Evolutionary Computing Systems Laboratory, Department of Computer Science 
# and Engineering, University of Nevada, Reno. 
#
# This file is part of OpenECSLENT 
#
#	OpenECSLENT is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	OpenECSLENT is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with OpenECSLENT.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------------
#-------------------------End Copyright Notice------------------------------

import pdb

from ogre.renderer.OGRE import Vector2
from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import ColourValue
from ogre.renderer.OGRE import FontManager

import ogre.renderer.OGRE as ogre
import ogre.io.OIS as OIS

from scripts.managers.graphics import GraphicsManager
from scripts.event import Event

#font = FontManager.getSingleton().create("StarWars", "General");
#FontManager.getSingleton().getByName("StarWars").load();

class _WidgetManager(object):
	_id = 0
	_recycler = []

	@staticmethod
	def _getNextId():
		newId = None
	
		if (len(_WidgetManager._recycler) == 0):
			newId = _WidgetManager._id + 1
			_WidgetManager._id = newId
		else:
			_WidgetManager._id = WidgetManager._recycler.pop()
	
		return newId
	
	@staticmethod
	def _recycleId(oldId):
		_WidgetManager._recycler.append(oldId)

class UIDefaults:
	POS = Vector2(0, 0)
	WIDTH = 100
	HEIGHT = 20

	PANEL_SIZE = Vector2(100, 20)
	PANEL_MATERIAL = ""
	LINE_SEP_MATERIAL  = ""

	LABEL_SIZE = Vector2(200, 50)
	LABEL_X_OFFSET = 5
	LABEL_Y_OFFSET = 5
	LABEL_TEXT_COLOR = Vector3(1.0, 1.0, 0.7)

	BUTTON_SIZE = Vector2(50, 20)
	BUTTON_OFF_TEXT_COLOR = Vector3(1.0, 1.0, 0.0)
	BUTTON_ON_TEXT_COLOR  = Vector3(0.0, 0.0, 0.5)

	MENU_ITEM_SIZE = Vector2(100, 20)
	MENU_DEFAULT_TEXT_COLOR = Vector3(1.0, 1.0, 0.0)
	MENU_SELECTED_TEXT_COLOR = Vector3(0.0, 0.0, 0.5)
	MENU_MATERIAL = ""

class Widget(object):
	"""
	Top level UI object
	"""
	def __init__(self, parent = None, position = Vector2(0, 0), size = Vector2(100, 13)):
		self._position = position
		self._size = size
		self.parent = parent
		
		self.events = type('', (), {})
		
		setattr(self.events, "onPositionChanged", Event())
		setattr(self.events, "onSizeChanged", Event())
		
		if (parent != None):
			parent.events.onPositionChanged.register(self.onPositionChanged)
			parent.events.onSizeChanged.register(self.onSizeChanged)

	@property
	def position(self):
		return self._position
	@position.setter
	def position(self, value):
		delta = self._position - value
		self._position = value
		
		self.events.onPositionChanged.fire(self, delta)
		
	def onPositionChanged(self, element, delta):
		self.events.onPositionChanged.fire(self, delta)
		
	def onSizeChanged(self, element, delta):
		self.events.onSizeChanged.fire(self, delta)
		
	@property
	def size(self):
		return self._size
	@size.setter
	def size(self, value):
		delta = self._size - value
		self._size = value
		
		self.events.onSizeChanged.fire(self, delta)

	@property
	def width(self):
		return self._size.x
	@width.setter
	def width(self, value):
		self.size.x = value

	@property
	def height(self):
		return self._size.y
	@height.setter
	def height(self, value):
		self.size.y = value

	@property
	def screenPosition(self):
		if self.parent:
			return self.parent.screenPosition + self.position
			
		return self.position

	def cursorInMe(self, mx, my):
		ax = self.screenPosition.x
		ay = self.screenPosition.y
		
		return ((mx >= ax) and (mx <= ax + self.width) and (my >= ay) and (my <= ay + self.height))

class UIOverlay(Widget):
	"""
	All UI elements live within this overlay.
	"""
	
	def __init__(self, ui = None):
		Widget.__init__(self)
		self.overlayManager = ogre.OverlayManager.getSingleton()
		self.panelId	 	= "UIPanel" + str(_WidgetManager._getNextId())
		self.panel	   		= self.overlayManager.createOverlayElement("Panel", self.panelId)
		self.panel.setMetricsMode(ogre.GMM_PIXELS)#RELATIVE_ASPECT_ADJUSTED)
		self.panel.setPosition(0, 0)
		width  = GraphicsManager.window.getWidth()
		height = GraphicsManager.window.getHeight() # VERY IMPORTANT or rayscene queries fail
		self.panel.setDimensions(width, height)
		if (ui != None):
			self.panel.setMaterialName(ui)
		self.panel.show()

		self.id = "MainUI"
		self.overlayName = "UIOverlay" + str(_WidgetManager._getNextId())
		self.overlay	 = self.overlayManager.create(self.overlayName)
		self.overlay.add2D(self.panel)
		self.overlay.show()
		self.panel.show()
		
	def show(self):
		self.overlay.show()
		self.panel.show()

	def hide(self):
		self.panel.hide()
		self.overlay.hide()
		
class Panel(Widget):
	class Placement:
		Below = 0
		Right = 1

	def __init__(self, parent = None, name = "panel", position = Vector2(0, 0), size = UIDefaults.PANEL_SIZE, material = ''):
		Widget.__init__(self, parent = parent, position = position, size = size)
		self.overlayManager = ogre.OverlayManager.getSingleton()

		self.panelId	= name + str(_WidgetManager._getNextId())
		self.panel	   	= self.overlayManager.createOverlayElement("Panel", self.panelId)
		self.panel.setMetricsMode(ogre.GMM_PIXELS)#RELATIVE_ASPECT_ADJUSTED)
		self.panel.setPosition(self.position[0], self.position[1])
		#self.panel.setDimensions(self.width, self.height)
		self.height = 0 # otherwise, the panel has extra height when using as a parent for labels and other widgets.
		self.panel.setDimensions(self.width, self.height)
		if material != None:
			self.panel.setMaterialName(material)

		self.panel.show()

		if self.parent == None:
			self.id = name
			self.overlayName = name + str(_WidgetManager._getNextId())
			self.overlay	 = self.overlayManager.create(self.overlayName)
			self.overlay.add2D(self.panel)
			self.overlay.show()
			#print "---------> Created overlay and added panel to overlay: ",  self.overlayName
		else:
			if (isinstance(parent, Panel)):
				self.parent.events.onShow.register(self.onShow)
				self.parent.events.onHide.register(self.onHide)
				
		self.events.onPositionChanged.register(self.onPositionChanged)

		self.panel.show()

		self.belowPosition = Vector2(0,0)
		self.rightPosition = Vector2(0,0)
		self.xgap = 0
		self.ygap = 0

		self.lheight = 1
		self._linePanels = [] # used by menu items
		self.addSep(0)
		
		setattr(self.events, "onShow", Event())
		setattr(self.events, "onHide", Event())
		
		self.events.onHide.post = True
		
		self._items = []

	def linePanel(self, material = ""):
		lid = self.panelId + "line/" + str(_WidgetManager._getNextId())
		lp = self.overlayManager.createOverlayElement("Panel", lid)
		lp.setMetricsMode(ogre.GMM_PIXELS)#RELATIVE_ASPECT_ADJUSTED)
		lp.setPosition(0, 0)
		lp.setDimensions(self.width, self.lheight)
		lp.setMaterialName(material)
		lp.show()
		
		return lp
 
	def addSep(self, y):
		lp = self.linePanel()
		self._linePanels.append(lp)
		lp.setPosition(0, y) # lineSep, 0, 0
		self.panel.addChild(lp)

	def adjustSeps(self):
		'''
		QUEST: What is this? - cmiles
		'''
		for lp in self._linePanels:
			lp.setDimensions(self.width, self.lheight)

	def show(self):
		if not self.parent:
			self.overlay.show()
		self.panel.show()
			
		self.events.onShow.fire(self)

	def hide(self):
		self.events.onHide.fire(self)
		
		self.panel.hide()
		if not self.parent:
			self.overlay.hide()

	def onPositionChanged(self, element, delta):
		self.panel.setPosition(self.position.x + delta.x, self.position.y + delta.y)
		
	def onSizeChanged(self, element, delta):
		self.events.onSizeChanged.fire(self, delta)

	def addItem(self, item, func = None, placement = Placement.Below):
		self._items.append(item)
		if(self.width < item.width):
			self.width = item.width
			self.adjustSeps()
			
		self.events.onShow.register(item, "onShow")
		self.events.onHide.register(item, "onHide")
		self.events.onPositionChanged.register(item, "onPositionChanged")
		self.events.onSizeChanged.register(item, "onSizeChanged")

		#pdb.set_trace()
		if placement == Panel.Placement.Below:
			item.position = self.belowPosition
			self.rightPosition = self.belowPosition + Vector2(item.width + self.xgap, 0)
			self.belowPosition = Vector2(0, self.belowPosition.y + item.height + self.ygap)

		elif placement == Panel.Placement.Right:
			item.pos = self.rightPosition
			self.rightPosition = self.rightPosition + Vector2(item.width + self.xgap, 0)

		if self.rightPosition.x > self.width:
			self.width = self.rightPosition.x

		if self.belowPosition.y > self.height:
			self.height = self.belowPosition.y

		self.panel.addChild(item.getOverlayElementToAdd())

		self.panel.setDimensions(self.width, self.height)
		self.addSep(self.height)

	def deleteItem(self, item):
		item.hide()
		self._item.remove(item)

	def getItems(self):
		return self._items

	def getOverlayElementToAdd(self):
		return self.panel


class Label(Widget):
	def __init__(self, parent = None, caption = "label", color = ColourValue(1.0, 1.0, 0.7, 1.0), size = UIDefaults.LABEL_SIZE, position = Vector2(0, 0)):
		Widget.__init__(self, parent = parent, position = position, size = size)
		self.overlayManager =  ogre.OverlayManager.getSingleton()
		self.caption = caption
		self.color   = color

		self.xOffset = 5
		self.yOffset = 2

		self.id = "ECSLENT/Label/" + str(self) + "/" + caption  + "/" + str(_WidgetManager._getNextId())
		self.textArea = self.overlayManager.createOverlayElement("TextArea", self.id)
		self.textArea.setMetricsMode(ogre.GMM_PIXELS)
		self.textArea.setCaption(caption)
		self.textArea.setPosition(self.position.x  + self.xOffset, self.position.y + self.yOffset)
		self.textArea.setDimensions(self.width, self.height)
		self.textArea.setFontName("StarWars")
		self.textArea.setCharHeight(self.height)
		self.textArea.setColour(self.color)
		self.textArea.show()
		
		if (parent != None):
			if (isinstance(parent, Panel)):
				parent.events.onShow.register(self.show)
				parent.events.onHide.register(self.hide)
		
#-------------------------------------------------------------------
	def getTextArea(self):
		return self.textArea
#-------------------------------------------------------------------
	def getOverlayElementToAdd(self):
		return self.textArea
#-------------------------------------------------------------------
	def show(self):
		self.textArea.show()
#-------------------------------------------------------------------
	def hide(self):
		self.textArea.hide()
		
	def onShow(self):
		self.textArea.show()
#-------------------------------------------------------------------
	def onHide(self):
		self.textArea.hide()
#-------------------------------------------------------------------
	def setCaption(self, caption):
		self.caption = caption
		self.textArea.setCaption(self.caption)
#-------------------------------------------------------------------
	def getCaption(self):
		return self.caption

	def onPositionChanged(self, element, delta):
		#print 'Label.posChanged', self.pos, self.screenPos
		self.textArea.setPosition(self.position.x + delta.x, self.position.y + delta.y)

	def onSizeChanged(self, element, delta):
		self.textArea.setDimensions(self.width + delta.x, self.height + delta.y)

	def setCharHeight(self, height):
		self.height = height
		self.setCharHeight(height)

class LabelPair(Panel):
	"""
	About 90% of the time we use labels in pairs
	the first is a fixed piece of text describing what the second is
	the second being the actual data we are representing - which varies over time
	"""
	def __init__(self, parent, label1Text, label2Text = '', columnWidths = Vector2(100,200), columnHeightPixels = UIDefaults.PANEL_SIZE.y):
		Panel.__init__(self, parent = parent, position = Vector2(0, 0), size = Vector2(columnWidths.x + columnWidths.y, columnHeightPixels))
		self.label1 = Label(parent=self, caption=label1Text, position=Vector2(0,0), size=Vector2(columnWidths.x, columnHeightPixels))
		self.addItem(self.label1, placement=Panel.Placement.Below)
		self.label2 = Label(parent=self, caption=label2Text, position=Vector2(columnWidths.x,0), size=Vector2(columnWidths.y, columnHeightPixels))
		self.addItem(self.label2, placement=Panel.Placement.Right)

	@property
	def leftCaption(self):
		self.label1.caption
	@leftCaption.setter
	def leftCaption(self, caption):
		self.label1.setCaption(caption)

	@property
	def rightCaption(self):
		self.label2.caption
	@rightCaption.setter
	def rightCaption(self, caption):
		self.label2.setCaption(caption)
		
	def onShow(self):
		self.label1.show()
		self.label2.show()
#-------------------------------------------------------------------
	def onHide(self):
		self.label1.hide()
		self.label2.hide()
		
	@property
	def caption(self):
		return self.caption
	@caption.setter
	def caption(self, caption):
		self.label2.setCaption(caption)

