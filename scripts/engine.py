#	
#	Engine
#
#		Fields
#		------------------------------
#
#			events.onStart
#			events.onStop
#			events.onTick
#
#			managers.
#				-	this is filled with all registered managers
#
#		Methods
#		------------------------------
#
#			register(manager)
#			unregister(manager)
#				-	these register/unregister managers to the Engine
#					registration will both add the manager to the engine and
#					register the manager to the engine events
#
#			start()
#				-	this runs the engine
#
#			stop()
#				-	this stops the engine
#
#		Engine Event Interface
#		------------------------------
#
#			def onStart(self)
#			def onStop(self)
#
#			def onTick(self, dtime)
#

from event import Event

class _Engine:
	#	initialization
	def __init__(self):
		self.managers = type('', (), {})
		
		self.events = type('', (), {})
		
		setattr(self.events, "onStart", Event())
		setattr(self.events, "onStop", Event())
		setattr(self.events, "onTick", Event())

		self.events.onStop.post = True
		
		self.runtime = 0
		
		self.running = False

	#	event registration
	def register(self, manager):
		setattr(self.managers, manager.__class__.__name__, manager)
		
		self.events.onStart.register(manager, "onStart")
		self.events.onStop.register(manager, "onStop")
		self.events.onTick.register(manager, "onTick")
		
	def unregister(self, manager):
		if hasattr(self.managers, manager.__class__.__name__):
			delattr(self.managers, manager.__class__.__name__)
			
			self.events.onStart.unregister(manager, "onStart")
			self.events.onStop.unregister(manager, "onStop")
			self.events.onTick.unregister(manager, "onTick")

	#	methods
	def stop(self):
		self.running = False
		
	def _stop(self):
		self.events.onStop.fire()

	def start(self):
		import time
		import ogre.renderer.OGRE as ogre
		
		weu = ogre.WindowEventUtilities() 	# Needed for linux/mac
		weu.messagePump()				 	# Needed for linux/mac

		oldTime = time.time()		
		
		now = None
		dtime = None
		
		self.running = True
		self.events.onStart.fire()

		while (self.running):
			now = time.time() 				# Change to time.clock() for windows
			dtime = now - oldTime
			oldTime = now
			
			self.events.onTick.fire(dtime)
			
			self.runtime += dtime
		
			weu.messagePump()			 	# Needed for linux/mac
			time.sleep(0.001)

		self._stop()
		
Engine = _Engine()
