#	
#	Entity
#
#		Fields
#		------------------------------
#
#			Entity.count
#
#			Entity.globalEvents.onCreate(self, entity)
#			entity.events.onDestroy(self, entity)
#			entity.events.onTick(self, entity, dtime)
#
#			entity.uiname
#			readonly entity.id
#			entity.mesh
#			entity.paused
#			entity.scale
#
#			entity.position
#			entity.absolutePosition
#			
#			entity.orientation
#			entity.absoluteOrientation
#			
#			entity.parent
#			readonly entity.parentAbsolutePosition
#			readonly entity.parentAbsoluteOrientation
#
#		Methods
#		------------------------------
#
#			Entity(self, uiname = None, mesh = None, scale = Vector3(1, 1, 1), parent = None, position = Vector3(0, 0, 0), orientation = Vector3(0, 0, 0))
#				-	only call from sub entities, do not instantiate this by itself
#
#			destroy(self)
#				-	only call from sub entities
#				-	never call a sub entities destruction except from other sub entities
#				-	when actually destroying an entity from the outside, use EntityManager
#
#			register(manager)
#			unregister(manager)
#				-	these register/unregister managers to the Entity Manager
#
#		Entity Event Interface
#		------------------------------
#
#			def onCreate(self, entity)
#			def onDestroy(self, entity)
#
#			def onTick(self, entity, dtime)
#

from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import Quaternion

from scripts.engine import Engine

from scripts.event import Event

import math

class Entity(object):
	globalEvents = type('', (), {})
	
	setattr(globalEvents, "onCreate", Event())

	#	entity manages its own counts
	count = 0
	_recycler = []

	@staticmethod
	def _createId():
		entityId = None

		if (Entity._recycler == []):
			entityId = Entity.count + 1
			Entity.count = entityId
		else:
			entityId = Entity._recycler.pop()

		return entityId

	@staticmethod
	def _recycleId(id):
		Entity._recycler.append(id)
	
	def __init__(self, uiname = None, mesh = None, scale = Vector3(1, 1, 1), parent = None, position = Vector3(0, 0, 0), orientation = Vector3(0, 0, 0)):
		self._id = Entity._createId()

		#	base entity fields
		if (uiname == None):
			uiname = str(self.id)
			
		orientation.z += math.pi

		self.uiname = uiname

		self.mesh = mesh
		self.scale = scale
		self.parent = parent
		self.position = position
		self.orientation = orientation
		
		self.events = type('', (), {})
		
		setattr(self.events, "onTick", Event())
		setattr(self.events, "onDestroy", Event())

		self.events.onDestroy.post = True
		
		self.globalEvents = Entity.globalEvents

		self._initialized = False
		self._destroyed = False
		self._paused = False

		Engine.events.onTick.register(self.onTick)

	def destroy(self):
		if (self._destroyed):
			return
			
		self._destroyed = True

		self.events.onDestroy.fire(self)

		Entity._recycleId(self._id)

		Engine.events.onTick.unregister(self.onTick)

	@property
	def id(self):
		return self._id

	@property
	def parentAbsolutePosition(self):
		if (self.parent == None):
			return 0

		return self.parent.absolutePosition

	@property
	def parentAbsoluteOrientation(self):
		if (self.parent == None):
			return 0

		return self.parent.parentAbsoluteOrientation

	@property
	def absolutePosition(self):
		return self.position + self.parentAbsolutePosition
	
	@absolutePosition.setter
	def absolutePosition(self, position):
		self.position = position - self.parentAbsolutePosition

	@property
	def absoluteOrientation(self):
		return self.orientation + self.parentAbsoluteOrientation

	@absoluteOrientation.setter
	def absoluteOrientation(self, orientation):
		self.orientation = orientation - self.parentAbsoluteOrientation
		
	def register(self, aspect):
		self.events.onTick.register(aspect, "onTick")
		self.events.onDestroy.register(aspect, "onDestroy")
		self.globalEvents.onCreate.register(aspect, "onCreate")
		
	def unregister(self, aspect):
		self.events.onTick.unregister(aspect, "onTick")
		self.events.onDestroy.unregister(aspect, "onDestroy")
		self.globalEvents.onCreate.unregister(aspect, "onCreate")

	@property
	def paused(self):
		return self._pause

	@paused.setter
	def paused(self, pause):
		if (self._pause != pause):
			self._pause = pause

			if (pause):
				Engine.events.onTick.unregister(self.onTick)
			else:
				Engine.events.onTick.register(self.onTick)
		
	def onTick(self, dtime):
		#	fire onCreate on first tick
		#	the reason for this is because the entity needs to be fully initialized
		#	before letting everyone else play with it
		if (not self._initialized):
			self._initialized = True

			self.globalEvents.onCreate.fire(self)

		self.events.onTick.fire(self, dtime)
