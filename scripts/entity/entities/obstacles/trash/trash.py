import random

from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import Quaternion

from scripts.entity.entity import Entity
from scripts.entity.aspects.physics import Physics
import scripts.managers.obstacle

from scripts.entity.entities.obstacles.obstacle import Obstacle

class Trash(Obstacle):
	def __init__(self, uiname, mesh, position = Vector3(0, 0, 0), orientation = Vector3(0, 0, 0), minSpeed = 0, maxSpeed = 0, minScale = 1, maxScale = 1, scaleFactor = 1):
		Obstacle.__init__(self, uiname, mesh, position, orientation, minSpeed, maxSpeed, minScale, maxScale)
