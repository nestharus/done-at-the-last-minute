#
#	Obstacle
#

import random

from ogre.renderer.OGRE import Vector3

from scripts.entity.entities.npc import NPC
from scripts.entity.aspects.outBounds import OutBounds

from scripts.utils.rotation import *

class Obstacle(NPC):
	def __init__(self, uiname, mesh, position = Vector3(0, 0, 0), orientation = Vector3(0, 0, 0), minSpeed = 0, maxSpeed = 0, minScale = 1, maxScale = 1):
		NPC.__init__(self, uiname, mesh, random.uniform(minScale, maxScale), position, orientation)
		
		OutBounds(self)
		
		self.speed = random.uniform(minSpeed, maxSpeed)
		self.desiredSpeed = self.speed

		orientation = self.orientation
		self.currentDirection = Vector3(orientation.x, orientation.y, orientation.z)
		self.desiredDirection = self.currentDirectionCopy
