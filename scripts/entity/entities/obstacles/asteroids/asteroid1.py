from ogre.renderer.OGRE import Vector3

from scripts.entity.entities.obstacles.asteroids.asteroid import Asteroid

class Asteroid1(Asteroid):
	def __init__(self, position = Vector3(0, 0, 0), orientation = Vector3(0, 0, 0)):
		Asteroid.__init__(self, "Astroid1", 'cube.mesh', position, orientation, minSpeed = 150, maxSpeed = 215, minScale = .5, maxScale = 1)
