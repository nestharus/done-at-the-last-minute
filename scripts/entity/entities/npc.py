#
#	NPC(Renderable)
#

from ogre.renderer.OGRE import Vector3

from scripts.entity.entities.renderable import Renderable
from scripts.entity.aspects.physics import Physics
from scripts.entity.aspects.ai import AI

class NPC(Renderable):
	def __init__(self, uiname, mesh, scale = 1, position = Vector3(0, 0, 0), orientation = Vector3(0, 0, 0)):
		Renderable.__init__(self, uiname, mesh, scale, None, position, orientation)
		
		Physics(self)
		AI(self)
