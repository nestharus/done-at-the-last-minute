import math

from ogre.renderer.OGRE import Vector3
from scripts.utils.rotation import *

from scripts.entity.entities.player import Player

class PlayerShip1(Player):
	def __init__(self, position = Vector3(0, 0, 0), orientation = Vector3(0, 0, 0)):
		Player.__init__(self, 'PLAYER_SHIP_1', 'cvn68.mesh', .5, position, orientation)
		
		self.acceleration = 80
		self.turnRate  = Vector3(0, deg2rad(95), 0)
		self.maxSpeed = 40*5
		self.minSpeed = 0
		
		self.overrideDirection = self.currentDirectionCopy
		self.orientation += Vector3(0, math.pi, 0)
