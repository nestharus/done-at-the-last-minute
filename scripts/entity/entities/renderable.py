#
#	Renderable(Entity)
#

from ogre.renderer.OGRE import Vector3

from scripts.entity.entity import Entity
from scripts.entity.aspects.renderer import Renderer

class Renderable(Entity):
	def __init__(self, uiname, mesh, scale = 1, parent = None, position = Vector3(0, 0, 0), orientation = Vector3(0, 0, 0)):
		scale = Vector3(scale, scale, scale)
	
		Entity.__init__(self, uiname, mesh, scale, parent, position, orientation)
		
		Renderer(self)
