#
#	Doodad(Renderable)
#

from ogre.renderer.OGRE import Vector3

from scripts.entity.entities.renderable import Renderable
from scripts.entity.aspects.physics import Physics

class Doodad(Renderable):
	def __init__(self, uiname, mesh, scale = 1, parent = None, position = Vector3(0, 0, 0), orientation = Vector3(0, 0, 0)):
		Renderable.__init__(self, uiname, mesh, scale, parent, position, orientation)
		
		Physics(self)
