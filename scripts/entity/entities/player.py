#
#	Player(Renderable)
#

from ogre.renderer.OGRE import Vector3

from scripts.entity.entities.renderable import Renderable

from scripts.entity.aspects.physics import Physics
from scripts.entity.aspects.bounds import Bounds
from scripts.entity.aspects.collision import Collision

from scripts.entity.entities.obstacles.asteroids.asteroid import Asteroid

class Player(Renderable):
	def _onPlayerCollide(self, entity, collidedEntity):
		if (isinstance(collidedEntity, Asteroid)):
			#print "collided with ", collidedEntity
			self.health -= 1
			collidedEntity.destroy()

	def _onPlayerDestroy(self, entity):
		self.events.onCollision.unregister(self._onPlayerCollide)
		self.events.onDestroy.unregister(self._onPlayerDestroy)

	def __init__(self, uiname, mesh, scale = 1, position = Vector3(0, 0, 0), orientation = Vector3(0, 0, 0)):
		Renderable.__init__(self, uiname, mesh, scale, None, position, orientation)
		
		Physics(self)
		Bounds(self)
		Collision(self)
		
		self.events.onCollision.register(self._onPlayerCollide)
		self.events.onDestroy.register(self._onPlayerDestroy)
		
		self.health = 3
