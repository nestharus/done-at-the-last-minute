from scripts.utils.bind import *

class OutBounds(object):
	#	bound properties
	def get_bounds(self):
		return self._bounds.bounds
		
	def set_bounds(self, bounds):
		self._bounds.bounds = bounds

	#	events
	def onTick(self, entity, dtime):
		if (self.bounds == None):
			return
			
		entityMin = entity.boundingBox.getMinimum()
		entityMax = entity.boundingBox.getMaximum()
		boundsMin = self.bounds.getMinimum()
		boundsMax = self.bounds.getMaximum()
			
		if (entityMax.x < boundsMin.x or entityMin.x > boundsMax.x or entityMax.z < boundsMin.z or entityMin.z > boundsMax.z):
			if (self.inBounds):
				entity.destroy()
		else:
			self.inBounds = True
		
	def onDestroy(self, entity):
		#	bounds storage
		unbind(entity, "_bounds", self)
		
		#	bounds events
		entity.events.onDestroy.unregister(self.onDestroy)
		entity.events.onTick.unregister(self.onTick)
		
		#	entity properties
		unbindProperty(entity, "bounds", self.get_bounds, self.set_bounds)

	#	creation
	def __init__(self, entity, bounds = None):
		if (hasattr(entity, "_bounds")):
			return
			
		#	bounds storage
		bind(entity, "_bounds", self)
		
		#	bounds events
		entity.events.onDestroy.register(self.onDestroy)
		entity.events.onTick.register(self.onTick)
		
		#	bounds properties
		self.bounds = bounds
		self.inBounds = False
		
		#	entity properties
		bindProperty(entity, "bounds", self.get_bounds, self.set_bounds)
