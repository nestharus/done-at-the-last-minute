#	
#	AI
#
#		This will add new fields and events to entities
#
#	Entity (additions)
#
#		Fields
#		------------------------------
#
#			commands
#
#		Methods
#		------------------------------
#
#			None
#
#		Entity Event Interface
#		------------------------------
#
#			def onChangeVisibility(self, entity, isVisible)
#			def onChangeParent(self, entity, oldParent, newParent)
#

from scripts.entity.entity import Entity

from scripts.utils.bind import *

class AI(object):
	def get_commands(self):
		return self._ai.commands

	def __init__(self, entity):
		if (hasattr(entity, "_ai")):
			return
	
		#	ai storage
		bind(entity, "_ai", self)
		
		#	ai events
		entity.events.onDestroy.register(self.onDestroy)
		entity.events.onTick.register(self.onTick)
		
		#	ai properties
		self.commands = []
		
		#	entity properties
		bindProperty(entity, "commands", self.get_commands, None)
		
	def onDestroy(self, entity):
		#	ai storage
		unbind(entity, "_ai", self)
		
		#	ai events
		entity.events.onDestroy.unregister(self.onDestroy)
		entity.events.onTick.unregister(self.onTick)
	
		#	entity properties
		unbindProperty(entity, "commands", self.get_commands, None)
		
	def onTick(self, entity, dtime):
		if (entity.commands != []):
			next(iter(entity.commands)).onTick(entity, dtime)
