from scripts.utils.bind import *

class Bounds(object):
	#	bound properties
	def get_bounds(self):
		return self._bounds.bounds
		
	def set_bounds(self, bounds):
		self._bounds.bounds = bounds

	#	events
	def onTick(self, entity, dtime):
		if (self.bounds == None):
			return
			
		entityMin = entity.boundingBox.getMinimum()
		entityMax = entity.boundingBox.getMaximum()
		boundsMin = self.bounds.getMinimum()
		boundsMax = self.bounds.getMaximum()
		
		pos = entity.position
		
		if (entityMin.x < boundsMin.x):
			pos.x -= (entityMin.x - boundsMin.x)
		elif (entityMax.x > boundsMax.x):
			pos.x -= (entityMax.x - boundsMax.x)
			
		if (entityMin.z < boundsMin.z):
			pos.z -= (entityMin.z - boundsMin.z)
		elif (entityMax.z > boundsMax.z):
			pos.z -= (entityMax.z - boundsMax.z)
			
		entity.position = pos
		
	def onDestroy(self, entity):
		#	bounds storage
		unbind(entity, "_bounds", self)
		
		#	bounds events
		entity.events.onDestroy.unregister(self.onDestroy)
		entity.events.onTick.unregister(self.onTick)
		
		#	entity properties
		unbindProperty(entity, "bounds", self.get_bounds, self.set_bounds)

	#	creation
	def __init__(self, entity, bounds = None):
		if (hasattr(entity, "_bounds")):
			return
			
		#	bounds storage
		bind(entity, "_bounds", self)
		
		#	bounds events
		entity.events.onDestroy.register(self.onDestroy)
		entity.events.onTick.register(self.onTick)
		
		#	bounds properties
		self.bounds = bounds
		
		#	entity properties
		bindProperty(entity, "bounds", self.get_bounds, self.set_bounds)
