#	
#	Physics
#
#		This will add new fields to entities
#
#		When the currentDirection is None, then the entity's direction
#		is its orientation. This will mean that it will always be moving
#		in the direction that it is facing.
#
#		If the current direction is something, then it will move in that direction.
#
#		All directions are of the type Quaternion
#
#		Methods
#		------------------------------
#
#			Physics(self, entity, speed = 0, acceleration = 0, turnRate = 0, currentDirection = None)
#				-	no direction means that the entity's direction is it's orientation
#					this will change the entity's orientation to match its desiredDirection
#					based on turnRate
#
#				-	when direction is present, the entity will simply move in that direction
#					the current direction does not factor in
#
#	Entity (additions)
#
#		Fields
#		------------------------------
#
#			entity.speed
#				-	scalar
#
#			entity.maxSpeed
#				-	scalar
#				-	negative implies no max speed
#
#			entity.desiredSpeed
#				-	scalar
#
#			entity.acceleration
#				-	scalar
#
#			entity.turnRate
#				-	scalar
#
#			entity.currentDirection
#				-	euler
#			entity.desiredDirection
#				-	euler
#
#			entity.overrideDirection
#				-	euler
#
#		Methods
#		------------------------------
#
#			
#

import math

from ogre.renderer.OGRE import Vector3

from scripts.utils.rotation import *
from scripts.utils.bind import *

class Physics(object):
	#	bound properties
	def get_speed(self):
		return self._physics.speed
		
	def set_speed(self, speed):
		if (self.maxSpeed >= 0 and speed > self.maxSpeed):
			speed = self.maxSpeed
			
		if (self.minSpeed >= 0 and speed < self.minSpeed):
			speed = self.minSpeed
			
		self._physics.speed = speed
		
	def get_maxSpeed(self):
		return self._physics.maxSpeed
		
	def set_maxSpeed(self, maxSpeed):
		self._physics.maxSpeed = maxSpeed
		
	def get_minSpeed(self):
		return self._physics.minSpeed
		
	def set_minSpeed(self, minSpeed):
		self._physics.minSpeed = minSpeed
		
	def get_desiredSpeed(self):
		return self._physics.desiredSpeed
		
	def set_desiredSpeed(self, desiredSpeed):
		self._physics.desiredSpeed = desiredSpeed
		
	def get_acceleration(self):
		return self._physics.acceleration
		
	def set_acceleration(self, acceleration):
		self._physics.acceleration = acceleration
	
	def get_turnRate(self):
		return self._physics.turnRate
		
	def set_turnRate(self, turnRate):
		self._physics.turnRate = turnRate
		
	def get_currentDirection(self):
		if (self._physics.currentDirection == None):
			return self.orientation
			
		return self._physics.currentDirection
		
	def set_currentDirection(self, currentDirection):
		if (self._physics.currentDirection == None):
			self.orientation = currentDirection
		else:
			self._physics.currentDirection = currentDirection
			
	def set_overrideDirection(self, overrideDirection):
		self._physics.currentDirection = overrideDirection
		
	def get_desiredDirection(self):
		return self._physics.desiredDirection
		
	def set_desiredDirection(self, desiredDirection):
		self._physics.desiredDirection = desiredDirection
		
	def get_currentDirectionCopy(self):
		direction = self.currentDirection

		return Vector3(direction.x, direction.y, direction.z)
		
	def get_velocity(self):
		return self._physics.speed*eulerVector2unitVector(self.currentDirection)
		
	def set_velocity(self, velocity):
		self._physics.speed = velocity.length()
		
		if (self._physics.speed == 0):
			return
			
		self.currentDirection = unitVector2eulerVector(velocity.normalisedCopy())
		
	def get_desiredVelocity(self):
		return self._physics.desiredSpeed*eulerVector2unitVector(self._physics.desiredDirection)
		
	def set_desiredVelocity(self, desiredVelocity):
		self._physics.desiredSpeed = desiredVelocity.length()
		
		if (self._physics.desiredSpeed == 0):
			return
	
		self._physics.desiredDirection = unitVector2eulerVector(velocity.normalisedCopy())
	
	#	private methods
	def updateSpeed(self, entity, dtime):
		acceleration = entity.acceleration*dtime
	
		if (acceleration == 0):
			pass
			
		#	update speed
		if (entity.speed < entity.desiredSpeed):
			if (entity.desiredSpeed - entity.speed < acceleration):
				entity.speed = entity.desiredSpeed
			else:
				entity.speed += acceleration
		else:
			if (entity.speed - entity.desiredSpeed < acceleration):
				entity.speed = entity.desiredSpeed
			else:
				entity.speed -= acceleration
				
	def updateDirection(self, entity, dtime):
		#print entity.currentDirection, " == ", entity.desiredDirection
		#if (entity.currentDirection != entity.desiredDirection):
		#print (eulerVectorRotation(entity.currentDirection, entity.desiredDirection, entity.turnRate*dtime))
		entity.currentDirection += eulerVectorRotation(entity.currentDirection, entity.desiredDirection, entity.turnRate*dtime)

	def updatePosition(self, entity, dtime):
		unit = eulerVector2unitVector(entity.currentDirection)
		unit.z = -unit.z
		entity.position = entity.position + unit*entity.speed*dtime

	#	events
	def onTick(self, entity, dtime):
		self.updateSpeed(entity, dtime)
		
		if (abs(entity.speed) < .5):
			return
				
		self.updateDirection(entity, dtime)
		self.updatePosition(entity, dtime)
		
	def onDestroy(self, entity):
		#	physics storage
		unbind(entity, "_physics", self)
		
		#	physics events
		entity.events.onDestroy.unregister(self.onDestroy)
		entity.events.onTick.unregister(self.onTick)
	
		#	entity properties
		unbindProperty(entity, "speed", self.get_speed, self.set_speed)
		unbindProperty(entity, "minSpeed", self.get_minSpeed, self.set_minSpeed)
		unbindProperty(entity, "maxSpeed", self.get_maxSpeed, self.set_maxSpeed)
		unbindProperty(entity, "desiredSpeed", self.get_desiredSpeed, self.set_desiredSpeed)
		unbindProperty(entity, "acceleration", self.get_acceleration, self.set_acceleration)
		unbindProperty(entity, "turnRate", self.get_turnRate, self.set_turnRate)
		unbindProperty(entity, "currentDirection", self.get_currentDirection, self.set_currentDirection)
		unbindProperty(entity, "currentDirectionCopy", self.get_currentDirectionCopy, None)
		unbindProperty(entity, "desiredDirection", self.get_desiredDirection, self.set_desiredDirection)
		unbindProperty(entity, "velocity", self.get_velocity, self.set_velocity)
		unbindProperty(entity, "desiredVelocity", self.get_desiredVelocity, self.set_desiredVelocity)

	#	creation
	def __init__(self, entity, speed = 0, acceleration = 0, turnRate = Vector3(0, 0, 0), currentDirection = None):
		if (hasattr(entity, "_physics")):
			return
			
		#	physics storage
		bind(entity, "_physics", self)
		
		#	physics events
		entity.events.onDestroy.register(self.onDestroy)
		entity.events.onTick.register(self.onTick)
		
		#	physics properties
		self.speed = speed
		self.minSpeed = -1
		self.maxSpeed = -1
		self.desiredSpeed = speed
		self.acceleration = acceleration
		self.turnRate = turnRate
		self.currentDirection = currentDirection
		self.desiredDirection = None
		
		if (self.currentDirection == None):
			self.desiredDirection = Vector3(entity.orientation.x, entity.orientation.y, entity.orientation.z)
		else:
			self.desiredDirection = Vector3(currentDirection.x, currentDirection.y, currentDirection.z)
		
		#	entity properties
		bindProperty(entity, "speed", self.get_speed, self.set_speed)
		bindProperty(entity, "minSpeed", self.get_minSpeed, self.set_minSpeed)
		bindProperty(entity, "maxSpeed", self.get_maxSpeed, self.set_maxSpeed)
		bindProperty(entity, "desiredSpeed", self.get_desiredSpeed, self.set_desiredSpeed)
		bindProperty(entity, "acceleration", self.get_acceleration, self.set_acceleration)
		bindProperty(entity, "turnRate", self.get_turnRate, self.set_turnRate)
		bindProperty(entity, "overrideDirection", None, self.set_overrideDirection)
		bindProperty(entity, "currentDirection", self.get_currentDirection, self.set_currentDirection)
		bindProperty(entity, "currentDirectionCopy", self.get_currentDirectionCopy, None)
		bindProperty(entity, "desiredDirection", self.get_desiredDirection, self.set_desiredDirection)
		bindProperty(entity, "velocity", self.get_velocity, self.set_velocity)
		bindProperty(entity, "desiredVelocity", self.get_desiredVelocity, self.set_desiredVelocity)
