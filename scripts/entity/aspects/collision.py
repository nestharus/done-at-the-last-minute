from scripts.event import Event
from scripts.managers.graphics import GraphicsManager

from scripts.entity.aspects.renderer import Renderer
#from scripts.entity.entities.obstacles.asteroids.asteroid import Asteroid
#from scripts.entity.entities.obstacles.trash.trash import Trash

from scripts.utils.bind import *

class Collision(object):
	#	events
	def onTick(self, entity, dtime):
		#	retrieve the ogre nodes intersecting with bounding box of current entity
		self.query.setBox(entity.boundingBox)
		collisionsOgre = self.query.execute().movables
		
		if (len(collisionsOgre) == 0):
			return
			
		collisions = []
		
		for collision in collisionsOgre:
			collision = Renderer.getEntity(collision)
			
			if (collision != None and collision != entity):
				collisions.append(collision)
			
		for collision in collisions:
			entity.events.onCollision.fire(entity, collision)
			
	def onDestroy(self, entity):
		#	collision storage
		unbind(entity, "_collision", self)
		
		#	collision events
		entity.events.onDestroy.unregister(self.onDestroy)
		entity.events.onTick.unregister(self.onTick)
		
		#	collision properties
		GraphicsManager.sceneManager.destroyQuery(self.query)
		
		#	entity properties
		delattr(entity.events, "onCollision")

	#	creation
	def __init__(self, entity):
		if (hasattr(entity, "_collision")):
			return
			
		#	collision storage
		bind(entity, "_collision", self)
		
		#	collision events
		entity.events.onDestroy.register(self.onDestroy)
		entity.events.onTick.register(self.onTick)
		
		#	collision properties
		self.query = GraphicsManager.sceneManager.createAABBQuery(entity.boundingBox)
		self.onCollision = Event()
		
		#	entity properties
		setattr(entity.events, "onCollision", self.onCollision)
