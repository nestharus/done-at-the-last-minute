#	
#	Renderer
#
#		This will render entities to the screen.
#
#
#		Fields
#		------------------------------
#
#			entity.isShown
#			entity.boundingBox -> AxisAlignedBox
#
#			entity.events.onChangeVisibility(self, entity, isShown)
#			entity.events.onChangeParent(self, entity, oldParent, newParent)
#
#		Methods
#		------------------------------
#
#			Renderer(self, entity)
#

from scripts.event import Event
from scripts.entity.entity import Entity
from scripts.managers.graphics import GraphicsManager

from scripts.utils.bind import *
from scripts.utils.rotation import *

class Renderer(object):
	def get_boundingBox(self):
		return self._renderer.ogreNode._getWorldAABB()

	def get_position(self):
		return self._renderer.position
		
	def set_position(self, position):
		renderer = self._renderer
		
		renderer.position = position
		renderer.ogreNode.setPosition(position)
		
		renderer.ogreNode._updateBounds()
		
	def get_orientation(self):
		return self._renderer.orientation
		
	def set_orientation(self, orientation):
		orientation.x %= (2*math.pi)
		orientation.y %= (2*math.pi)
		orientation.z %= (2*math.pi)
		
		#orientation = unitVector2eulerVector(eulerVector2unitVector(orientation))
		
		renderer = self._renderer
		renderer.ogreNode.setOrientation(eulerVector2quaternion(orientation))
		renderer.orientation = orientation
		
		renderer.ogreNode._updateBounds()

	#	need a field to store flag
	def get_isShown(self):
		return self._renderer.isShown

	def set_isShown(self, isShown):
		isShown = self._renderer
		
		if (isShown != renderer.isShown):
			renderer._isShown = isShown
		
			if (isShown):
				renderer.parentNode.removeChild(renderer.ogreNode)
			else:
				renderer.parentNode.addChild(renderer.ogreNode)
				
			self.events.onChangeVisbility.fire(self, isShown)

	#	need a field to store engine entity
	def get_parent(self):
		return self._renderer.parent

	def set_parent(self, parent):
		renderer = self._renderer
	
		if (parent != renderer.parent):
			oldParent = renderer.parent
			newParent = parent
			renderer.parent = newParent
		
			position = self.absolutePosition
		
			if (renderer.isShown):
				renderer.parentNode.removeChild(renderer.ogreNode)
				
			parentNode = None
		
			if (parentEntity == None):
				parentNode = GraphicsManager.sceneManager.getRootSceneNode()
			else:
				parentNode = parentEntity._renderer.ogreNode
				
			renderer.parentEntity = parentEntity
			renderer.parentNode = parentNode
			
			if (renderer.isShown):
				renderer.parentNode.addChild(renderer.ogreNode)
				
			self.absolutePosition = position
			
			self.events.onChangeParent.fire(self, oldParent, newParent)
			
			if (renderer.parentNode != None):
				renderer.parentNode._updateBounds()
				
			renderer.ogreNode._updateBounds()

	#	no need for field
	def get_scale(self):
		return self._renderer.ogreNode.getScale()

	def set_scale(self, scale):
		self._renderer.ogreNode.scale(scale, scale, scale)
		self._renderer.ogreNode._updateBounds()

	def register(self, aspect):
		#	original register is put into the aspect
		self._renderer._register(aspect)
		
		self.events.onChangeVisibility.register(aspect, "onChangeVisibility")
		self.events.onChangeParent.register(aspect, "onChangeParent")
		
	def unregister(self, aspect):
		#	original unregister is put into the aspect
		self._renderer._unregister(aspect)
		
		self.events.onChangeVisibility.unregister(aspect, "onChangeVisibility")
		self.events.onChangeParent.unregister(aspect, "onChangeParent")
		
	_entities = {}
		
	@staticmethod
	def getEntity(ogreEntity):
		return Renderer._entities.get(ogreEntity.getName())
			
	def __init__(self, entity):
		if (hasattr(entity, "_renderer")):
			return
	
		#	renderer storage
		bind(entity, "_renderer", self)

		#	renderer properties
		self.isShown = True
		self.parent = entity.parent
		self.position = entity.position
		self.orientation = entity.orientation
		
		if (entity.parent != None):
			self.parentNode = entity.parent._renderer.ogreNode
		else:
			self.parentNode = GraphicsManager.sceneManager.getRootSceneNode()
		
		self.ogreEntity = GraphicsManager.sceneManager.createEntity(entity.uiname + "_ogreEntity" + str(entity.id), entity.mesh)
		self.ogreNode = self.parentNode.createChildSceneNode(entity.uiname + "_ogreNode" + str(entity.id), entity.position)
		self.ogreNode.attachObject(self.ogreEntity)
		self.ogreNode.setOrientation(eulerVector2quaternion(entity.orientation))
		self.ogreNode.scale(entity.scale)
		#self.ogreNode.showBoundingBox(True)
		
		self.ogreNode._updateBounds()
		
		Renderer._entities[self.ogreEntity.getName()] = entity

		#	renderer events
		entity.events.onDestroy.register(self.onDestroy)
		
		#	entity properties
		bindProperty(entity, "isShown", self.get_isShown, self.set_isShown)
		bindProperty(entity, "parent", self.get_parent, self.set_parent)
		bindProperty(entity, "scale", self.get_scale, self.set_scale)
		bindProperty(entity, "position", self.get_position, self.set_position)
		bindProperty(entity, "orientation", self.get_orientation, self.set_orientation)
		bindProperty(entity, "boundingBox", self.get_boundingBox, None)
		
		#	these fields are stored because they may not be active
		#	for the entity
		#
		#	if they are not active and renderer doesn't store them, there is
		#	no way to unbind them
		self._register = entity.register
		self._unregister = entity.unregister
		bind(entity, "register", self.register)
		bind(entity, "unregister", self.unregister)
		
		self.onChangeVisibility = Event()
		self.onChangeParent = Event()
		setattr(entity.events, "onChangeVisibility", self.onChangeVisibility)
		setattr(entity.events, "onChangeParent", self.onChangeParent)
		
	def onDestroy(self, entity):
		Renderer._entities.pop(self.ogreEntity.getName())
	
		#	renderer properties
		self.ogreEntity.detachFromParent()	
		
		GraphicsManager.sceneManager.destroyEntity(self.ogreEntity)
		self.ogreNode.detachAllObjects()
		if (GraphicsManager.sceneManager.hasSceneNode(self.ogreNode.getName())):
			GraphicsManager.sceneManager.destroySceneNode(self.ogreNode)
		
		#	renderer events
		entity.events.onDestroy.unregister(self.onDestroy)
		
		#	entity properties
		unbindProperty(entity, "isShown", self.get_isShown, self.set_isShown)
		unbindProperty(entity, "parent", self.get_parent, self.set_parent)
		unbindProperty(entity, "scale", self.get_scale, self.set_scale)
		unbindProperty(entity, "position", self.get_position, self.set_position)
		unbindProperty(entity, "orientation", self.get_orientation, self.set_orientation)
		unbindProperty(entity, "boundingBox", self.get_boundingBox, None)
		
		unbind(entity, "register", self.register)
		unbind(entity, "unregister", self.unregister)
		
		delattr(entity.events, "onChangeVisibility")
		delattr(entity.events, "onChangeParent")

		#	copy renderer properties back to entity
		entity.position = self.position
		entity.orientation = self.orientation
		entity.parent = self.parent
		
		#	renderer storage
		unbind(entity, "_renderer", self)
