from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import Quaternion

from scripts.utils.rotation import *

from scripts.entity.commands.command import Command

class Follow(Command):
	def __init__(self, entity, target):
		Command.__init__(self, entity)
		
		self.target = target
		
		# how close the entity will get to the target before stopping
		# entity continues to follow the target indefinitely
		self.leash = 100
		
	def onTick(self, entity, dtime):
		entity.desiredDirection = unitVector2eulerVector(entity.position - self.target.position, entity.currentDirection.z)
		
		# when the entity is within range, it will begin to slow down
		if (entity.acceleration == 0):
			return
			
		slowDownProximity = entity.speed*entity.speed/(2*entity.acceleration)
		distance = entity.position.distance(self.targetPosition) - self.leash
		
		if (distance > slowDownProximity):
			entity.desiredSpeed = entity.maxSpeed
		else:
			entity.desiredSpeed = 0
