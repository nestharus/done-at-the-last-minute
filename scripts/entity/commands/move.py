from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import Quaternion

from scripts.utils.rotation import *

from scripts.entity.commands.command import Command

class Move(Command):
	def __init__(self, entity, targetPosition):
		Command.__init__(self, entity)
		
		self.targetPosition = targetPosition
		
		self.error = .1
		
	def onTick(self, entity, dtime):
		entity.desiredDirection = unitVector2eulerVector(entity.position - self.targetPosition, entity.currentDirection.z)
		
		if (entity.acceleration == 0):
			pass
			
		# when the entity is within range, it will begin to slow down
		slowDownProximity = entity.speed*entity.speed/(2*entity.acceleration)
		distance = entity.position.distance(self.targetPosition)
		
		if (distance > slowDownProximity):
			entity.desiredSpeed = entity.maxSpeed
		else:
			entity.desiredSpeed = 0
			
			if (distance < self.error):
				entity.commands.remove(self)
