from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import Quaternion

from scripts.utils.rotation import *

from scripts.entity.commands.command import Command

class Intercept(Command):
	def __init__(self, entity, target):
		Command.__init__(self, entity)
		
		self.target = target
		
		self.error = 10
		
	def onTick(self, entity, dtime):
		relativeSpeed = (self.target.velocity - entity.velocity).length()
		distance = self.entity.position.distance(self.target.position)
		
		time = 1000
		if (relativeSpeed != 0):
			time = distance/relativeSpeed
			
		targetPosition = self.target.position + self.target.speed*time
		
		entity.desiredDirection = unitVector2eulerVector(entity.position - self.target.position, entity.currentDirection.z)
		
		# when the entity is within range, it will begin to slow down
		if (entity.acceleration == 0):
			return
			
		slowDownProximity = entity.speed*entity.speed/(2*entity.acceleration)
		distance = entity.position.distance(self.targetPosition)
		
		if (distance > slowDownProximity):
			entity.desiredSpeed = entity.maxSpeed
		else:
			entity.desiredSpeed = 0
			
			if (distance < self.error):
				entity.commands.remove(self)
