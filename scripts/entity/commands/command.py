import math

from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import Quaternion

from scripts.utils.rotation import *

from scripts.entity.aspects.ai import AI
from scripts.entity.aspects.physics import Physics
from scripts.entity.aspects.renderer import Renderer

class Command:
	def __init__(self, entity):
		entity.commands.append(self)
		
	def onTick(self, entity, dtime):
		pass
