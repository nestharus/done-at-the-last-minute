#
#	InputManager
#
#		Supports Mouse and Keyboard with Buffered/Unbuffered Input
#
#		Use the following to for unbuffered input
#
#			inputManager.keyboard.isKeyDown(OIS.)
#			inputManager.mouse.getMouseState().X/Y.abs/rel
#
#		Fields
#		------------------------------
#
#			events.onStart(self)
#			events.onStop(self)
#			events.onTick(self, dtime)
#
#			events.onKeyPressed(self, frameEvent)
#			events.onKeyReleased(self, frameEvent)
#			events.onMouseMoved(self, frameEvent)
#			events.onMousePressed(self, frameEvent)
#			events.onMouseReleased(self, frameEvent)
#
#		Methods
#		------------------------------
#
#			register(self)
#			unregister(self)
#

import ogre.renderer.OGRE as ogre
import ogre.io.OIS as OIS

from scripts.engine import Engine
from scripts.event import Event
from scripts.managers.graphics import GraphicsManager

class _InputManager(OIS.MouseListener, OIS.KeyListener):
	def __init__(self):
		Engine.register(self)
	
		#	initialize input listeners
		OIS.KeyListener.__init__(self)
		OIS.MouseListener.__init__(self)
	
		#	initialize fields
		self.inputManager = None
		
		self.keyboard = None
		self.mouse = None
		
		self.events = type('', (), {})
		
		setattr(self.events, "onStart", Event())
		setattr(self.events, "onStop", Event())
		setattr(self.events, "onTick", Event())

		self.events.onStop.post = True
		
		setattr(self.events, "onKeyPressed", Event())
		setattr(self.events, "onKeyReleased", Event())
		setattr(self.events, "onMouseMoved", Event())
		setattr(self.events, "onMousePressed", Event())
		setattr(self.events, "onMouseReleased", Event())
		
	def register(self, input):
		self.events.onStart.register(input, "onStart")
		self.events.onStop.register(input, "onStop")
		self.events.onTick.register(input, "onTick")
		
		self.events.onKeyPressed.register(input, "onKeyPressed")
		self.events.onKeyReleased.register(input, "onKeyReleased")
		self.events.onMouseMoved.register(input, "onMouseMoved")
		self.events.onMousePressed.register(input, "onMousePressed")
		self.events.onMouseReleased.register(input, "onMouseReleased")
		
	def unregister(self, input):
		self.events.onStart.unregister(input, "onStart")
		self.events.onStop.unregister(input, "onStop")
		self.events.onTick.unregister(input, "onTick")
		
		self.events.onKeyPressed.unregister(input, "onKeyPressed")
		self.events.onKeyReleased.unregister(input, "onKeyReleased")
		self.events.onMouseMoved.unregister(input, "onMouseMoved")
		self.events.onMousePressed.unregister(input, "onMousePressed")
		self.events.onMouseReleased.unregister(input, "onMouseReleased")
		
	def onStart(self):
		#retrieve the rendered window
		renderWindow = GraphicsManager.root.getAutoCreatedWindow()
		
		#initialize input system
		windowHandle = renderWindow.getCustomAttributeUnsignedLong("WINDOW")
		paramList = [("WINDOW", str(windowHandle))]
		paramList.extend([("x11_mouse_grab", "false")])
		paramList.extend([("x11_mouse_hide", "false")])
		paramList.extend([("x11_keyboard_grab", "false")])
		paramList.extend([("XAutoRepeatOn", "true")])
		
		self.inputManager = OIS.createPythonInputSystem(paramList)
		
		# initialize input objects
		self.keyboard = None
		self.mouse = None
		
		try:
			self.keyboard = self.inputManager.createInputObjectKeyboard(OIS.OISKeyboard, True)
		except Exception:
			pass
			
		try:
			self.mouse = self.inputManager.createInputObjectMouse(OIS.OISMouse, True)
		except Exception:
			pass
			
		if (self.keyboard):
			self.keyboard.setEventCallback(self)
			
		if (self.mouse):
			self.mouse.setEventCallback(self)
			
		self.events.onStart.fire()
		
	def onStop(self):
		self.events.onStop.fire()
	
		if self.keyboard:
			self.inputManager.destroyInputObjectKeyboard(self.keyboard)
		if self.mouse:
			self.inputManager.destroyInputObjectMouse(self.mouse)
		
		OIS.InputManager.destroyInputSystem(self.inputManager)
		
		#	clear fields
		self.keyboard = None
		self.mouse = None
		
		self.inputManager = None
		
	def onTick(self, dtime):
		if (self.keyboard):
			self.keyboard.capture()
		if (self.mouse):
			self.mouse.capture()
			
		self.events.onTick.fire(dtime)

	### Key Listener callbacks ###
	def keyPressed(self, frameEvent):
		self.events.onKeyPressed.fire(frameEvent)
		
		return True

	def keyReleased(self, frameEvent):
		self.events.onKeyReleased.fire(frameEvent)
		
		return True
		
	### Mouse Listener callbacks ###
	def mouseMoved(self, frameEvent):
		self.events.onMouseMoved.fire(frameEvent)
		
		return True

	def mousePressed(self, frameEvent, id):
		self.events.onMousePressed.fire(frameEvent)
		
		return True

	def mouseReleased(self, frameEvent, id):
		self.events.onMouseReleased.fire(frameEvent)
		
		return True
		
InputManager = _InputManager()
