from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import Vector2
from ogre.renderer.OGRE import AxisAlignedBox

import ogre.renderer.OGRE as ogre

from scripts.engine import Engine
from scripts.managers.entity import EntityManager

from scripts.entity.entities.players.playerShip1 import PlayerShip1
from scripts.entity.entities.obstacles.asteroids.asteroid1 import Asteroid1

from scripts.managers.obstacle import ObstacleManager
from scripts.managers.entity import EntityManager
from graphics import GraphicsManager

import scripts.widget

from scripts.utils.rotation import *
import math

#AxisAlignedBox (Real mx, Real my, Real mz, Real Mx, Real My, Real Mz)

#	entity.getBoundingRadius()

class _GameManager(object):
	def __init__(self):
		Engine.register(self)

	def onStart(self):
		#	the things here never change

		#	initialize camera
		self._camera = GraphicsManager.sceneManager.createCamera("Camera")
		self._camera.nearClipDistance = 5
		
		self._cameraHeight = 1500
		self._cameraNode = GraphicsManager.rootNode.createChildSceneNode('CameraNode', (0, -self._cameraHeight, 0))
		
		self._cameraNodeYaw = self._cameraNode.createChildSceneNode('CameraNodeYaw')
		#self._cameraNodeYaw.yaw(180/57.2957795131)

		self._cameraNodePitch = self._cameraNodeYaw.createChildSceneNode('CameraNodePitch')
		self._cameraNodePitch.pitch(90/57.2957795131)
		self._cameraNodePitch.attachObject(self._camera)

		GraphicsManager.camera = self._camera

		#	initialize map bounds
		self.width = 2225 #- 300
		self.height = 1250 #- 300
		
		self.bounds = AxisAlignedBox(-self.width/2, 0, -self.height/2, self.width/2, 0, self.height/2)
		
		#	initialize terrain
		self._groundPlane = ogre.Plane((0, -1, 0), -100)
		meshManager = ogre.MeshManager.getSingleton()
		meshManager.createPlane('Ground', 'General', self._groundPlane, self.width, self.height, 20, 20, True, 1, 1, 1, (0, 0, 1))

		self._ground = GraphicsManager.sceneManager.createEntity('GroundEntity', 'Ground')
		GraphicsManager.rootNode.createChildSceneNode('GroundNode').attachObject(self._ground)
		self._ground.castShadows = False
		self._groundMaterial = None

		#	initialize lighting
		GraphicsManager.sceneManager.setAmbientLight((1,1,1))

		self._player = None

		self.loadLevel()

	def loadLevel(self):
		self.game1()

	@property
	def player(self):
		return self._player

	@property
	def ground(self):
		return self._groundMaterial

	@ground.setter
	def ground(self, materialName):
		self._groundMaterial = materialName
		self._ground.setMaterialName(materialName)

	#def onTick(self, dtime):
		#if (self.player.getAbsolute

	def game1(self):
		#	initialize player
		self._player = PlayerShip1()
		self._player.bounds = self.bounds
		self._overlay = scripts.widget.Panel()
		self._timer = scripts.widget.LabelPair(self._overlay, "Remaining Time: ", "60")
		self._health = scripts.widget.LabelPair(self._overlay, "Health: ", str(self._player.health))
		self._overlay.addItem(self._timer)
		self._overlay.addItem(self._health)
		self._timer.position = Vector2(-550, 0)
		self._health.position = Vector2(-550, 0)
		self.remainingTime = 120
		
		Engine.events.onTick.register(self.onTick)

		#	initialize obstacles
		ObstacleManager.minPeriod = .5
		ObstacleManager.maxPeriod = 1
		ObstacleManager.minGen = 1
		ObstacleManager.maxGen = 2
		
		ObstacleManager.bounds = self.bounds

		ObstacleManager.obstacleTypes.append(Asteroid1)
		#ObstacleManager.obstacleTypes.append(Asteroid2)

		self.ground = 'Examples/SpacePlane'
		
	def onTick(self, dtime):
		if (self._player == None):
			return
	
		self.remainingTime -= dtime
		
		if (self.remainingTime <= 0):
			self._health.rightCaption = str(int(self._player.health))
		
			Engine.events.onTick.unregister(self.onTick)
			
			self._timer.rightCaption = "You Win!"
			ObstacleManager.maxPeriod = -1
			
			EntityManager.paused = True
		elif (self._player.health <= 0):
			self._health.rightCaption = str(int(self._player.health))
		
			Engine.events.onTick.unregister(self.onTick)
			
			self._timer.rightCaption = "You Lose!"
			ObstacleManager.maxPeriod = -1
			
			EntityManager.paused = True
			
			self._player.destroy()
			self._player = None
		else:
			self._timer.rightCaption = str(int(self.remainingTime))
			self._health.rightCaption = str(int(self._player.health))
		
GameManager = _GameManager()
