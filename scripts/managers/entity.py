#	
#	EntityManager
#
#		Fields
#		------------------------------
#
#			events.onStart(self)
#			events.onStop(self)
#
#			entities
#
#		Methods
#		------------------------------
#
#			register(manager)
#			unregister(manager)
#

from scripts.engine import Engine
from scripts.event import Event
from scripts.entity.entity import Entity

from scripts.utils.bind import *

class _EntityManager:
	#	initialization
	def __init__(self):
		Engine.register(self)

		Entity.globalEvents.onCreate.register(self._onCreateEntity)
		
		self.events = type('', (), {}) 
		
		setattr(self.events, "onStart", Event())
		setattr(self.events, "onStop", Event())
		setattr(self.events, "onTick", Event())

		self.events.onStop.post = True
		
		self.entities = None

		self._pause = None
		self.paused = property(fget = self._getPause, fset = self._setPause)
	
	#	event registration
	def register(self, callback):
		self.events.onStart.register(callback, "onStart")
		self.events.onStop.register(callback, "onStop")
		self.events.onStop.register(callback, "onTick")
		
	def unregister(self, callback):
		self.events.onStart.unregister(callback, "onStart")
		self.events.onStop.unregister(callback, "onStop")
		self.events.onStop.register(callback, "onTick")

	#	engine events
	def onStart(self):
		self._pause = False
		self.entities = []
		self.entitiesToRemove = []
		
		self.events.onStart.fire()
		
	def onStop(self):
		self.events.onStop.fire()
		
		for entity in self.entities:
			entity.destroy()

		self.entities = None
		self._pause = None

	def _getPause(self):
		return self._pause

	def _setPause(self, pause):
		if (self._pause != pause):
			self._pause = pause

			for entity in self.entities:
				entity.paused = pause

				if (pause):
					self.events.onTick.unregister(entity.onTick)
				else:
					self.events.onTick.register(entity.onTick)

	#	make entities run through this manager instead engine
	#	this is to make sure that entities aren't dispersed all over the place
	def _onDestroyEntity(self, entity):
		self.entities.remove(entity)
		self.events.onTick.unregister(entity.onTick)
		
		unbindProperty(entity, "paused", self._getPause, self._setPause)
		
	def _onCreateEntity(self, entity):
		self.entities.append(entity)

		entity.events.onDestroy.register(self._onDestroyEntity)

		Engine.events.onTick.unregister(entity.onTick)
		self.events.onTick.register(entity.onTick)
		
		bindProperty(entity, "paused", self._getPause, self._setPause)
		
	def onTick(self, dtime):
		self.events.onTick.fire(dtime)
		
EntityManager = _EntityManager()
