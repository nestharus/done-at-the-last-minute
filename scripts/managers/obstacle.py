#
#	ObstacleManager
#
#		obstacles include trash and astroids
#
#		Fields
#		------------------------------
#
#			events.onStart(self)
#			events.onStop(self)
#			events.onReset(self)
#			events.onTick(self, dtime)
#
#			
#
#			ObstacleManager.minPeriod
#			ObstacleManager.maxPeriod
#				-	how often to spawn obstacles
#
#			ObstacleManager.obstacleTypes = []
#				-	what types of obstacles to spawn
#				-	fill this with more of the same time to do weights
#
#		Methods
#		------------------------------
#
#			reset(self)
#
#				clears period, timer, and obstacle types
#

from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import AxisAlignedBox

from scripts.entity.aspects.physics import Physics
import scripts.entity.aspects.renderer

from scripts.engine import Engine
from scripts.event import Event

from scripts.utils.rotation import *

import random
import math

class _ObstacleManager:
	def __init__(self):
		self.bounds = None

		Engine.register(self)

		self.events = type('', (), {})

		setattr(self.events, "onStart", Event())
		setattr(self.events, "onStop", Event())
		setattr(self.events, "onReset", Event())
		setattr(self.events, "onTick", Event())

		self.events.onStop.post = True
		self.events.onReset.post = True

		self.obstacleTypes = None
		self.minPeriod = None
		self.maxPeriod = None
		self.minGen = None
		self.maxGen = None
		self._period = None
		self._time = None

	def register(self, obj):
		self.events.register(obj, "onStart")
		self.events.register(obj, "onStop")
		self.events.register(obj, "onTick")

	def unregister(self, obj):
		self.events.unregister(obj, "onStart")
		self.events.unregister(obj, "onStop")
		self.events.unregister(obj, "onTick")

	def onStart(self):
		#	this is filled by the level
		#	more of a given item gives it more weight
		self.obstacleTypes = []
		self.minPeriod = -1
		self.maxPeriod = -1
		self.minGen = 0
		self.maxGen = 0
		self._period = -1
		self._time = 0

		self.events.onStart.fire()
	
	def onStop(self):
		self.events.onStop.fire()
		self.obstacleTypes = None
		self.minPeriod = None
		self.maxPeriod = None
		self.minGen = None
		self.maxGen = None
		self._period = None
		self._time = None

	def reset(self):
		self.events.onReset.fire()
		self.obstacleTypes = None
		self._period = None
		self._time = None

	def _calculatePosition(self):
		minimum = self.bounds.getMinimum()
		maximum = self.bounds.getMaximum()
	
		x = None
		y = None
		orientation = None

		minOrientation = None
		maxOrientation = None

		#	unravel the rectangle into a line and get position along that line
		position = random.randint(0, 3)

		#	  R		L	T	 B
		#	|----|----|----|----|
		if (position < 2):
			#	R/L
			y = random.uniform(minimum.z, maximum.z)

			if (position == 0):
				#	R
				x = maximum.x
				minOrientation = deg2rad(180 - 45)
				maxOrientation = deg2rad(180 + 45)
				
				if (y < 0):
					maxOrientation = deg2rad(180)
				else:
					minOrientation = deg2rad(180)
			else:
				#	L
				x = minimum.x
				minOrientation = deg2rad(-45)
				maxOrientation = deg2rad(45)
				
				if (y < 0):
					minOrientation = 0
				else:
					maxOrientation = 0
		else:
			#	T/B
			x = random.uniform(minimum.x, maximum.x)

			if (position == 2):
				#	T
				y = maximum.z
				minOrientation = deg2rad(270 - 45)
				maxOrientation = deg2rad(270 + 45)
				
				if (x < 0):
					minOrientation = deg2rad(270)
				else:
					maxOrientation = deg2rad(270)
			else:
				#	B
				y = minimum.z
				minOrientation = deg2rad(90 - 45)
				maxOrientation = deg2rad(90 + 45)
				
				if (x < 0):
					maxOrientation = deg2rad(90)
				else:
					minOrientation = deg2rad(90)
				
		return (x, y, random.uniform(minOrientation, maxOrientation))

	def onTick(self, dtime):
		if (self.minPeriod < 0 or self.maxPeriod < 0 or self.bounds == None or self.minPeriod > self.maxPeriod):
			return

		if (self._period < 0):
			self._period = random.uniform(self.minPeriod, self.maxPeriod)

		self._time += dtime

		if (self._time >= self._period and len(self.obstacleTypes) > 0):
			i = random.randint(self.minGen, self.maxGen)
			obstacle = None
			
			while (i > 0):
				if (len(self.obstacleTypes) > 0):
					obstacle = random.choice(self.obstacleTypes)

				if (obstacle != None):
					position = self._calculatePosition()
				
					#	obstacle should define its own random speed and scale
					entity = obstacle(Vector3(position[0], 0, -position[1]), Vector3(0, position[2], 0))
					entity.bounds = self.bounds
					maximum = entity.boundingBox.getMaximum()
					minimum = entity.boundingBox.getMinimum()
					size = maximum - minimum
					size.y = 0
					size.x = -size.x
					entity.position += size*eulerVector2unitVector(entity.orientation)
					
				i -= 1

			self.events.onTick.fire(self._time)
			self._time = 0
			self._period = random.uniform(self.minPeriod, self.maxPeriod)

ObstacleManager = _ObstacleManager()
