import ogre.renderer.OGRE as ogre

from scripts.engine import Engine
from scripts.event import Event

'''
RaySceneQuery * 	createRayQuery (const Ray &ray, uint32 mask)
 	Creates a RaySceneQuery for this scene manager. More...
 	
void 	findNodesIn (const AxisAlignedBox &box, list< SceneNode * >::type &list, SceneNode *exclude=0)
 	Recurses the octree, adding any nodes intersecting with the box into the given list. More...
 
void 	findNodesIn (const Sphere &sphere, list< SceneNode * >::type &list, SceneNode *exclude=0)
 	Recurses the octree, adding any nodes intersecting with the sphere into the given list. More...
 
void 	findNodesIn (const PlaneBoundedVolume &volume, list< SceneNode * >::type &list, SceneNode *exclude=0)
 	Recurses the octree, adding any nodes intersecting with the volume into the given list. More...
 
void 	findNodesIn (const Ray &ray, list< SceneNode * >::type &list, SceneNode *exclude=0)
 	Recurses the octree, adding any nodes intersecting with the ray into the given list. More...
'''

class _GraphicsManager(object):
	def __init__(self):
		Engine.register(self)
		
	def onStart(self):
		self.createRoot()
		self.defineResources()
		self.setupRenderSystem()
		self.createRenderWindow()
		self.initializeResourceGroups()
		self.setupSceneBase()
		
	def onStop(self):
		del self.root
		
	def onTick(self, dtime):
		self.root.renderOneFrame()
		
	def createRoot(self):
		self.root = ogre.Root()
		
	def defineResources(self):
		cf = ogre.ConfigFile()
		cf.load("resources.cfg")

		seci = cf.getSectionIterator()
		while seci.hasMoreElements():
			secName = seci.peekNextKey()
			settings = seci.getNext()

			for item in settings:
				typeName = item.key
				archName = item.value
				ogre.ResourceGroupManager.getSingleton().addResourceLocation(archName, typeName, secName)
				
	def setupRenderSystem(self):
		if not self.root.restoreConfig() and not self.root.showConfigDialog():
			raise Exception("User canceled the config dialog -> Application.setupRenderSystem()")

	# Create the render window
	def createRenderWindow(self):
		self.root.initialise(True, "Main Render Window")
		self.renderTarget = self.root.getRenderTarget("Main Render Window")
		
	def initializeResourceGroups(self):
		ogre.TextureManager.getSingleton().setDefaultNumMipmaps(5)
		ogre.ResourceGroupManager.getSingleton().initialiseAllResourceGroups()

	@property
	def camera(self):
		return self.viewport.getCamera()

	@camera.setter
	def camera(self, camera):
		self.viewport.setCamera(camera)

		if (camera != None):
			camera.setAspectRatio(float(self.width)/float(self.height))

	@property
	def width(self):
		return self.viewport.getActualWidth()

	@property
	def height(self):
		return self.viewport.getActualHeight()

	@property
	def rootNode(self):
		return self.sceneManager.getRootSceneNode()

	@property
	def window(self):
		return self.root.getAutoCreatedWindow()
		
	def setupSceneBase(self):
		#	initialize scene manager
		self.sceneManager = self.root.createSceneManager("OctreeSceneManager", "Default SceneManager")

		#	initialize viewport
		self.viewport = self.window.addViewport(None)
		self.viewport.setBackgroundColour((0, 0, 0))

GraphicsManager = _GraphicsManager()
