from ogre.renderer.OGRE import Vector3
from ogre.renderer.OGRE import Quaternion

import ogre.renderer.OGRE as ogre
import ogre.io.OIS as OIS

import math

from scripts.utils.rotation import *
from scripts.engine import Engine
from scripts.managers.input import InputManager
from scripts.managers.game import GameManager

class _ControlManager:
	def __init__(self):
		InputManager.register(self)
		
		#self.stop = None
		#self.coast = None
		
	#def onStart(self):
		#self.stop = False
		#self.coast = True
		
	def onTick(self, dtime):
		if (GameManager.player == None):
			return
	
		keyboard = InputManager.keyboard
		entity = GameManager.player
		
		speed = 0
		yaw = 0
		
		if (keyboard.isKeyDown(OIS.KC_DOWN)):
			speed -= 1
		
		if (keyboard.isKeyDown(OIS.KC_UP)):
			speed += 1
			
		if (keyboard.isKeyDown(OIS.KC_LEFT)):
			yaw -= 1
		
		if (keyboard.isKeyDown(OIS.KC_RIGHT)):
			yaw += 1
			
		if (yaw != 0 and entity.speed != 0):
			vec = Vector3(0, yaw*entity.turnRate.y*dtime, 0)
			entity.orientation += vec
			entity.currentDirection += vec
			entity.desiredDirection += vec
			
		if (speed != 0):
			entity.speed += entity.acceleration*speed*dtime
			entity.desiredSpeed = entity.speed
		
	### Key Listener callbacks ###
	def onKeyPressed(self, frameEvent):
		pass
		'''
		if (frameEvent.key == OIS.KC_SPACE and not self.stop):
			entity = GameManager.player
			entity.desiredDirection = entity.getCurrentDirection()
			entity.desiredSpeed = 0
			self.stop = True
		'''

	'''
	### Mouse Listener callbacks ###
	def mouseMoved(self, frameEvent):
		pass

	def mousePressed(self, frameEvent, id):
		if (id == OIS.MB_Right):
			if (len(self.selectionMgr.selectedEntities) == 0):
				return

			addCommand = self.inputMgr.keyboard.isKeyDown(OIS.KC_LSHIFT) or self.inputMgr.keyboard.isKeyDown(OIS.KC_RSHIFT)
			if (not addCommand):
				for entity in self.selectionMgr.selectedEntities:
					entity.commands = []
				
			ms = self.inputMgr.mouse.getMouseState()

			ms.width = self.engine.gfxMgr.viewport.actualWidth
			ms.height = self.engine.gfxMgr.viewport.actualHeight
			mousePos = (ms.X.abs/float(ms.width), ms.Y.abs/float(ms.height))
			mouseRay = self.engine.gfxMgr.camera.getCameraToViewportRay(mousePos[0], mousePos[1])
			result = mouseRay.intersects(self.engine.gfxMgr.groundPlane)

			if (result.first):
				pos = mouseRay.getPoint(result.second)
				pos.y = 0

				distanceSquared = None
				closest = None
				closestDistance = self.selectionMgr.selectionRadius*self.selectionMgr.selectionRadius
				
				for entity in self.entityMgr.entities:
					distanceSquared = entity.pos.squaredDistance(pos)
					
					if (distanceSquared < closestDistance):
						closest = entity
						closestDistance = distanceSquared

				if closest:
					intercept = self.inputMgr.keyboard.isKeyDown(OIS.KC_LCONTROL)
					
					if (intercept):
						for entity in self.selectionMgr.selectedEntities:
							entity.commands.append(Intercept(entity, closest))
					else:
						for entity in self.selectionMgr.selectedEntities:
							entity.commands.append(Follow(entity, closest))
				else:
					for entity in self.selectionMgr.selectedEntities:
						entity.commands.append(Move(entity, pos))

	def mouseReleased(self, frameEvent, id):
		pass
	'''
		
ControlManager = _ControlManager()
