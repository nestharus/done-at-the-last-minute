#
#	NetworkManager
#
#		NA
#
#		Fields
#		------------------------------
#
#			None
#
#		Methods
#		------------------------------
#
#			None
#

from scripts.engine import Engine
from scripts.event import Event

class _NetworkManager:
    def __init__(self):
		Engine.register(self)

NetworkManager = _NetworkManager()
